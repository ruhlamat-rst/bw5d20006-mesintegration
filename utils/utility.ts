import _ from 'lodash';
import bunyan from 'bunyan';
import EventEmitter from 'events';
import Config from '../config/config.json';

export const emitter = new EventEmitter.EventEmitter();
const log = bunyan.createLogger({ name: 'utility', level: 20 });
/** @constant {Object} */
export const DATA_TYPE = {
  STRING: 'string',
  NUMBER: 'number',
  BOOLEAN: 'boolean',
  OBJECT: 'object',
};

/** @constant {Object} */
export const HTTP_STATUS_CODE = {
  SUCCESS: 200,
  ACCEPTED: 202,
  BAD_REQUEST: 406,
  NOT_ACCEPTABLE: 406,
  INTERNAL_SERVER_ERROR: 500,
};
export let requestTimeout = {
  timeout: 45 * 1000,
};
/**
 * Efficiently calculates the comma separated string
 * passed into the method. The input is expected in below format,
 *
 * concat("This","is","an","example") return "Thisisanexample"
 *
 * @param {string} strings comma separated strings.
 */
export const concat = (...strings: string[]) => {
  return _.reduce(strings, (accumulator, currentItem) => {
    return accumulator + currentItem;
  });
};

/**
 * Checks if give configuration parameter exists with given data types. If no then exit node js service
 * pointing deficiency in perticular parameter.
 *
 * @param {string} configParam
 * @param {string} dataType
 */
export const checkIfExists = (
  configParam: any,
  configParamString: any,
  dataType: any
) => {
  // check if configuration parameter exists in configuration file.
  if (typeof configParam != 'boolean' && !configParam) {
    log.fatal(
      'Configuration parameter is invalid OR absent: ' + configParamString
    );
    process.exit(1);
  }
  // check if configuration parameter has valid data type.
  if (typeof configParam != dataType) {
    log.fatal(
      "Data type for configuration parameter '" +
        configParamString +
        "' must be: " +
        dataType
    );
    process.exit(1);
  }
};
/**
 * validate the configuration parameter is valid with given conditions
 *
 */
export const validateConfigfileParameters = (CONFIG = Config) => {
  log.info('Validating Configuration file.');

  checkIfExists(CONFIG.industryid, 'CONFIG.industryid', DATA_TYPE.NUMBER);
  checkIfExists(CONFIG.loginType, 'CONFIG.loginType', DATA_TYPE.STRING);

  checkIfExists(CONFIG.server, 'CONFIG.server', DATA_TYPE.OBJECT);
  checkIfExists(
    CONFIG.server.protocol,
    'CONFIG.server.protocol',
    DATA_TYPE.STRING
  );
  checkIfExists(CONFIG.server.host, 'CONFIG.server.host', DATA_TYPE.STRING);
  checkIfExists(CONFIG.server.port, 'CONFIG.server.port', DATA_TYPE.NUMBER);

  checkIfExists(CONFIG.socketio, 'CONFIG.socketio', DATA_TYPE.OBJECT);
  checkIfExists(CONFIG.socketio.host, 'CONFIG.socketio.host', DATA_TYPE.STRING);
  checkIfExists(CONFIG.socketio.port, 'CONFIG.socketio.port', DATA_TYPE.NUMBER);
  checkIfExists(
    CONFIG.socketio.namespace,
    'CONFIG.socketio.namespace',
    DATA_TYPE.STRING
  );
  checkIfExists(
    CONFIG.socketio.eventname,
    'CONFIG.socketio.eventname',
    DATA_TYPE.STRING
  );

  checkIfExists(CONFIG.credential, 'CONFIG.credential', DATA_TYPE.OBJECT);
  checkIfExists(
    CONFIG.credential.password,
    'CONFIG.credential.password',
    DATA_TYPE.STRING
  );
  checkIfExists(
    CONFIG.credential.identifier,
    'CONFIG.credential.identifier',
    DATA_TYPE.STRING
  );

  checkIfExists(CONFIG.kafkaEdge, 'CONFIG.kafkaEdge', DATA_TYPE.OBJECT);
  checkIfExists(
    CONFIG.kafkaEdge.host,
    'CONFIG.kafkaEdge.host',
    DATA_TYPE.STRING
  );
  checkIfExists(
    CONFIG.kafkaEdge.port,
    'CONFIG.kafkaEdge.port',
    DATA_TYPE.NUMBER
  );
  checkIfExists(
    CONFIG.kafkaEdge.autoCommit,
    'CONFIG.kafkaEdge.autoCommit',
    DATA_TYPE.BOOLEAN
  );
  checkIfExists(
    CONFIG.kafkaEdge.fetchMinBytes,
    'CONFIG.kafkaEdge.fetchMinBytes',
    DATA_TYPE.NUMBER
  );
  checkIfExists(
    CONFIG.kafkaEdge.fetchMaxBytes,
    'CONFIG.kafkaEdge.fetchMaxBytes',
    DATA_TYPE.NUMBER
  );

  checkIfExists(CONFIG.logger, 'CONFIG.logger', DATA_TYPE.OBJECT);
  checkIfExists(
    CONFIG.logger.loglevel,
    'CONFIG.logger.loglevel',
    DATA_TYPE.NUMBER
  );

  checkIfExists(CONFIG.elements, 'CONFIG.elements', DATA_TYPE.OBJECT);
  checkIfExists(
    CONFIG.elements.substation,
    'CONFIG.elements.substation',
    DATA_TYPE.STRING
  );
  checkIfExists(
    CONFIG.elements.order,
    'CONFIG.elements.order',
    DATA_TYPE.STRING
  );
  checkIfExists(
    CONFIG.elements.orderproduct,
    'CONFIG.elements.orderproduct',
    DATA_TYPE.STRING
  );
  checkIfExists(
    CONFIG.elements.orderrecipe,
    'CONFIG.elements.orderrecipe',
    DATA_TYPE.STRING
  );
  checkIfExists(
    CONFIG.elements.orderroadmap,
    'CONFIG.elements.orderroadmap',
    DATA_TYPE.STRING
  );
  checkIfExists(
    CONFIG.elements.checkin,
    'CONFIG.elements.checkin',
    DATA_TYPE.STRING
  );
  checkIfExists(
    CONFIG.elements.checkout,
    'CONFIG.elements.checkout',
    DATA_TYPE.STRING
  );
  checkIfExists(
    CONFIG.elements.component,
    'CONFIG.elements.component',
    DATA_TYPE.STRING
  );
  checkIfExists(
    CONFIG.elements.partstatus,
    'CONFIG.elements.partstatus',
    DATA_TYPE.STRING
  );
  checkIfExists(
    CONFIG.elements.rework,
    'CONFIG.elements.rework',
    DATA_TYPE.STRING
  );
  checkIfExists(
    CONFIG.elements.bomdetails,
    'CONFIG.elements.bomdetails',
    DATA_TYPE.STRING
  );
  checkIfExists(
    CONFIG.elements.componentcheck,
    'CONFIG.elements.componentcheck',
    DATA_TYPE.STRING
  );
  checkIfExists(
    CONFIG.elements.productionimage,
    'CONFIG.elements.productionimage',
    DATA_TYPE.STRING
  );
  checkIfExists(
    CONFIG.elements.productionimageinfo,
    'CONFIG.elements.productionimageinfo',
    DATA_TYPE.STRING
  );

  checkIfExists(CONFIG.defaults, 'CONFIG.defaults', DATA_TYPE.OBJECT);
  checkIfExists(
    CONFIG.defaults.isPreviousDataIgnored,
    'CONFIG.defaults.isPreviousDataIgnored',
    DATA_TYPE.BOOLEAN
  );

  checkIfExists(CONFIG.status, 'CONFIG.status', DATA_TYPE.OBJECT);
  checkIfExists(
    CONFIG.status.running,
    'CONFIG.status.running',
    DATA_TYPE.STRING
  );

  log.info('Configuration file successfully validated.');
};

/**
 * Throw Error.
 *
 * @param {object} reply
 * @param {object} err
 */
export const throwError = (reply: any, err: any) => {
  reply.code(500).send({ message: err });
};

/**
 * This function map the data with element schema
 * @param {object} data which contains actual data to log
 * @param {object} fields which contains schema of elements
 */
export const assignDataToSchema = function (data: any, fields: any[]) {
  let postData: any = {};
  // log.info(" Map values with element schema ");
  // map values with element schmea
  // if prefix required for any element need to pass it to function as a param
  let prefix = '';
  if (fields.length > 0) {
    for (let i = 0; fields && i < fields.length; i++) {
      let tagname = prefix + fields[i].tagName;
      let choice = fields[i].emgTagType || '';
      switch (choice) {
        case 'Int':
        case 'Double':
        case 'Float':
        case 'Long':
          if (data[tagname] || data[tagname] === 0) {
            postData[fields[i].tagName] =
              data[tagname] || data[tagname] === 0
                ? +data[tagname]
                : data[tagname];
          }
          break;
        case 'String':
          if (data[tagname] || data[tagname] === '') {
            let stringValue = data[tagname]
              ? data[tagname].toString()
              : data[tagname];
            stringValue = stringValue
              ? stringValue.replace(/\u0000/g, '')
              : stringValue;
            postData[fields[i].tagName] = stringValue
              ? stringValue.trim()
              : stringValue;
          }
          break;
        case 'Boolean':
          postData[fields[i].tagName] = data[tagname] || false;
          break;
        default:
          if (data[tagname] || data[tagname] == 0) {
            postData[fields[i].tagName] = data[tagname];
          }
      }
    }
  } else {
    postData = data;
  }
  return postData;
};
export const checkSessionExpired = (data: any) => {
  if (data && data.errors && data.errors.errorCode === 'INVALID_SESSION') {
    log.error(`Session Id expired ${JSON.stringify(data.errors)}`);
    emitter.emit('sessionExpired');
  }
};

export const arrayUniqueByKey = (data: any, key: any) => {
  const unique = [
    ...new Map(data.map((item: any) => [item[key], item])).values(),
  ];
  return unique;
};

export const cloneDeep = (data: any) => {
  return JSON.parse(JSON.stringify(data));
};

/**
 * When any API failed or need to wait before calling next function or step use this function
 * @param {Number} time Refer for adding delay time
 */
export async function setTimer(time: number) {
  time = time || 1; // default time is 1 seconds if time not passed from function
  await new Promise((resolve) => setTimeout(resolve, time * 1000));
}
