'use strict';

import elementService from '../service/element.service';
import bunyan from 'bunyan';
import EventEmitter from 'events';
import Config from '../config/config.json';
import * as Utility from './utility';
function masterdata(config = Config, utility = Utility, tags: any) {
  const log = bunyan.createLogger({
    name: 'Master Data',
    level: config.logger.loglevel,
  });
  const elements = config.elements;
  const defaults = config.defaults;
  const parameterTags = tags.parametertags;
  const substationTags = tags.substationtags;
  let retryServerTimer = defaults.retryServerTimer || 10; // in seconds

  return {
    stationInfo: [],
    /**
     * This method all the recipes from recipedetails element
     */
    async getStationInformation() {
      const elementName = elements.substation || 'substation';
      try {
        /*
                    query = `query=lineid==1%26%26sublineid=="subline-1"%26%26sublineid=="sublineid-1"&sortquery=createdTimestamp==-1&pagenumber=1&pagesize=1`
                */
        let query = `query=${[substationTags.SUBSTATIONID_TAG]}=="${
          config.substationid
        }"`;
        query += `&sortquery=createdTimestamp==-1&pagenumber=1&pagesize=1`;
        const response = await elementService.getElementRecords(
          elementName,
          query
        );
        if (
          response.data &&
          response.data.results &&
          response.data.results.length
        ) {
          log.error(`station fetched successfully`);
          this.stationInfo = response.data.results;
        } else {
          log.error(
            `station not found for elementName : ${elementName} ${JSON.stringify(
              response.data
            )}`
          );
          utility.checkSessionExpired(response.data);
          await utility.setTimer(retryServerTimer);
          await this.getStationInformation();
        }
      } catch (ex) {
        log.error(`Exception to fetch recipe for element : ${elementName}`);
        const messageObject = ex.response ? ex.response.data : ex;
        log.error(messageObject);
        await utility.setTimer(retryServerTimer);
        await this.getStationInformation();
      }
    },
  };
}

export const MasterDataJS = {
  masterdata,
};
