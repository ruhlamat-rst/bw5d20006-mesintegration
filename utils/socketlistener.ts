'use strict';

import bunyan from 'bunyan';
import EventEmitter from 'events';
import socket from 'socket.io-client';
import Config from '../config/config.json';

let emitter = new EventEmitter.EventEmitter();
function socketlistener(config = Config) {
  let log = bunyan.createLogger({
    name: 'Socket Listener',
    level: config.logger.loglevel,
  });
  let socketio = config.socketio;
  let server = config.server;
  return {
    init: function () {
      log.info(`${socketio.host} : ${socketio.port}`);
      let defaultSocket = socket(
        `${server.protocol}://${socketio.host}:${socketio.port}`
      );
      defaultSocket.on('connect', () => {
        log.info(`Default update_<elementName> Socket connected `);
      });
      defaultSocket.on('disconnect', () => {
        log.error(`Default update_<elementName> Socket disconnected `);
      });
      // defaultSocket.on('update_businesshours', (data: any) => {
      //   emitter.emit('businesshours', data);
      // });
      // defaultSocket.on('update_businessholidays', (data: any) => {
      //   emitter.emit('businessholidays', data);
      // });
      defaultSocket.on('update_recipe', (data: any) => {
        emitter.emit('recipe', data);
      });
      defaultSocket.on('update_order', (data: any) => {
        emitter.emit('order', data);
      });

      defaultSocket.on('update_substation', (data: any) => {
        emitter.emit('substation', data);
      });
      defaultSocket.on('update_parameters', (data: any) => {
        emitter.emit('parameters', data);
      });
      defaultSocket.on('update_stationstatus', (data: any) => {
        emitter.emit('stationstatus', data);
      });
      defaultSocket.on('update_cyclerunning', (data: any) => {
        emitter.emit('cyclerunning', data);
      });
      // defaultSocket.on('update_checkin', (data: any) => {
      //   emitter.emit('checkin', data);
      // });
      defaultSocket.on('update_checkout', (data: any) => {
        emitter.emit('checkout', data);
      });
      defaultSocket.on('update_personelcheck', (data: any) => {
        emitter.emit('personelcheck', data);
      });
      //TODO: Add equipmentalarm element
      // defaultSocket.on('update_equipmentalarm', (data: any) => {
      //   emitter.emit('equipmentalarm', data);
      // });
      // call socketNamespace
      if (socketio.namespace) {
        this.socketNamespace();
      }
    },
    // swx namespace socket listener
    socketNamespace: function () {
      let namespace = socketio.namespace;
      let eventName = socketio.eventname;
      let socketNamespace = socket(
        `${server.protocol}://${socketio.host}:${socketio.port}/${namespace}`
      );
      socketNamespace.on('connect', () => {
        emitter.emit('connect', 'connect');
        log.info(`SWX analyzer_<eventname> Socket connected `);
      });
      socketNamespace.on('disconnect', () => {
        log.error(`SWX analyzer_<eventname> Socket disconnected `);
      });
      socketNamespace.on(`${namespace}_${eventName}`, (data: any) => {
        emitter.emit(`${eventName}`, data);
      });
      // PLC Error
      socketNamespace.on(`${namespace}_${eventName}plcerror`, (error: any) => {
        emitter.emit(`${eventName}plcerror`, error);
      });

      socketNamespace.on(`${namespace}_recipedownload`, (data: any) => {
        emitter.emit('recipedownload', data);
      });

      socketNamespace.on(`${namespace}_recipeupload`, (data: any) => {
        emitter.emit('recipeupload', data);
      });

      socketNamespace.on(`${namespace}_parameterupload`, (data: any) => {
        emitter.emit('parameterupload', data);
      });
    },
  };
}

export const SocketListenerJS = {
  emitter,
  socketlistener,
};
