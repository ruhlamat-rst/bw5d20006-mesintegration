import { Request, Response, NextFunction } from 'express';
import elementService from '../service/element.service';
import CONFIG from '../config/config.json';

class Controller {
  constructor() {}
  async commitOrder(req: Request, res: Response, next: NextFunction) {
    const {
      ProductionLineNo,
      Quantity,
      WipOrderNo,
      ProductNo,
      RecipeName,
    } = req.body;
    // TODO: Add new order to shopworx tags?
    const elementName = CONFIG.elements.order || 'order';
  }
  async commitComponentLifeAlarm(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    // TODO: post alarm to shopworx
  }
}

export default new Controller();
