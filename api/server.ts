import cors from 'cors';
import * as Utility from '../utils/utility';
import Config from '../config/config.json';
import bunyan from 'bunyan';
import express, { Request, Response, NextFunction } from 'express';
import 'express-async-errors';
import BaseRouter from './routes';

let log = bunyan.createLogger({
  name: 'Api Server',
  level: Config.logger.loglevel,
});
const app = express();

app.use(express.json());
app.use(express.urlencoded());
app.use(cors);

// routers
app.use('/api', BaseRouter);
// Print API errors
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  log.error(err.message, err);
  return res.status(Utility.HTTP_STATUS_CODE.BAD_REQUEST).json({
    error: err.message,
  });
});

export default { app, log };
