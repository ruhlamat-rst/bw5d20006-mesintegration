import { Router } from 'express';
import Controller from './controller';

// Init router and path
const router = Router();

// Add sub-routes
router.post('/post-order', Controller.commitOrder);
router.post('/post-componentlifealarm', Controller.commitComponentLifeAlarm);

// Export the base-router
export default router;
