import bunyan from 'bunyan';
import qs from 'querystring';
import TAGS from '../tags/tags.json';
import CONFIG from '../config/config.json';
import * as UTILITY from '../utils/utility';
import INTERFACE from '../service/interface.service';
import KafkaService from '../service/kafka.service';
import moment from 'moment';
import elementService from '../service/element.service';
import kafkaService from '../service/kafka.service';

function handshake() {
  const log = bunyan.createLogger({
    name: 'mes-handshake',
    level: CONFIG.logger.loglevel,
  });

  return {
    communication: -1,
    online: -1,
    async inithandshake() {
      try {
        const response = await INTERFACE.handshake();
        // log.error(`handshake response ${response.status}`);
        if (response.status === UTILITY.HTTP_STATUS_CODE.SUCCESS) {
          if (this.communication == 0 || this.communication == -1) {
            this.communication = 1;
            if (this.online == 0 || this.online == -1) {
              kafkaService.consumer?.resume();
              this.online = 1;
            }
            log.error(`consumer start`);
            // stop mesdowntime
            // this.updateRecordInSWX();
          }
        } else if (
          response.status === UTILITY.HTTP_STATUS_CODE.INTERNAL_SERVER_ERROR
        ) {
          if (this.online == 1 || this.online == -1) {
            kafkaService.consumer?.pause();
            log.error(`consumer closed`);

            this.online = 0;
          }

          if (this.communication == 1 || this.communication == -1) {
            this.communication = 0;
            // start mesdowntime
            // this.writeRecordInSWX();
          }
          await UTILITY.setTimer(10);
          this.inithandshake();
        } else {
          if (this.communication == 1 || this.communication == -1) {
            this.communication = 0;
            // start mesdowntime
            // this.writeRecordInSWX();
          }
        }
        await UTILITY.setTimer(10);
        this.inithandshake();
      } catch (error) {
        if (this.online == 1 || this.online == -1) {
          kafkaService.consumer?.pause();
          log.error(`consumer closed`);

          this.online = 0;
        }

        if (this.communication == 1 || this.communication == -1) {
          this.communication = 0;
          // start mesdowntime
          // this.writeRecordInSWX();
        }
        await UTILITY.setTimer(10);
        this.inithandshake();
      }
    },
    /**
     * This method write the handshake result in ShopWorx database
     * @param {Object} plcdata
     */
    async writeRecordInSWX() {
      let payload = {
        state: 'In Progress',
        starttime: moment().valueOf(),
        assetid: CONFIG.assetid,
      };

      log.trace(`mesoffline Payload ${JSON.stringify(payload)}`);
      const elementName = CONFIG.elements.mesoffline || 'mesoffline';
      try {
        //check in progress first
        let query = `query=state=="${qs.escape('In Progress')}"`;

        const inProgressed = await elementService.getElementRecords(
          elementName,
          query
        );
        if (inProgressed.status === UTILITY.HTTP_STATUS_CODE.SUCCESS) {
          if (
            inProgressed.data &&
            inProgressed.data.results &&
            inProgressed.data.results.length <= 0
          ) {
            const response = await elementService.createElementRecord(
              elementName,
              payload
            );
            if (response.status === UTILITY.HTTP_STATUS_CODE.SUCCESS) {
              log.error(
                `Record saved successfully in ShopWorx for cyclerunning ${JSON.stringify(
                  response.data
                )}`
              );
            } else {
              log.error(
                `Error in writing cyclerunning data in ShopWorx ${JSON.stringify(
                  response.data
                )}`
              );
              UTILITY.checkSessionExpired(response.data);
              await UTILITY.setTimer(CONFIG.defaults.retryServerTimer);
              await this.writeRecordInSWX();
            }
          }
        } else {
          log.error(
            `Error in writing cyclerunning data in ShopWorx ${JSON.stringify(
              inProgressed.data
            )}`
          );
          UTILITY.checkSessionExpired(inProgressed.data);
          await UTILITY.setTimer(CONFIG.defaults.retryServerTimer);
          await this.writeRecordInSWX();
        }
      } catch (error) {
        log.error(
          `Exception in writing cyclerunning data in ShopWorx ${error}`
        );
        await UTILITY.setTimer(CONFIG.defaults.retryServerTimer);
        await this.writeRecordInSWX();
      }
    },
    /**
     * This method update the mesoffline record to completed in ShopWorx database
     * @param {Object} plcdata
     */
    async updateRecordInSWX() {
      let payload = {
        state: 'Completed',
        endtime: moment().valueOf(),
        assetid: CONFIG.assetid,
      };

      const elementName = CONFIG.elements.mesoffline || 'mesoffline';
      try {
        let query = `query=state=="${qs.escape('In Progress')}"`;

        const response = await elementService.updateElementRecordsByQuery(
          elementName,
          payload,
          query
        );
        if (response.status === UTILITY.HTTP_STATUS_CODE.SUCCESS) {
          log.error(
            `Record update successfully in ShopWorx ${JSON.stringify(
              response.data
            )}`
          );
        } else {
          log.error(
            `Error in update cyclerunning in ShopWorx ${JSON.stringify(
              response.data
            )}`
          );
          // check authentication
          UTILITY.checkSessionExpired(response.data);
          await UTILITY.setTimer(CONFIG.defaults.retryServerTimer);
          await this.updateRecordInSWX();
        }
      } catch (error) {
        log.error(
          `Exception in updating cyclerunning data in ShopWorx ${error}`
        );
        await UTILITY.setTimer(CONFIG.defaults.retryServerTimer);
        await this.updateRecordInSWX();
      }
    },
  };
}

export default { handshake };
