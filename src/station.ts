import _ from 'lodash';
import bunyan from 'bunyan';
import TAGS from '../tags/tags.json';
import CONFIG from '../config/config.json';
import * as UTILITY from '../utils/utility';
import INTERFACE from '../service/interface.service';
import { EventEmitter } from 'events';
import moment from 'moment';
import elementService from '../service/element.service';
import KafkaService from '../service/kafka.service';
import parameters from './parameters';
import e from 'express';

function stations(
  config = CONFIG,
  substation: any,
  utility = UTILITY,
  tags = TAGS,
  emitter: EventEmitter,
  kafka: typeof KafkaService
) {
  const parameterTags = tags.parametertags;
  const stationTags = tags.substationtags;
  let lineid = substation[stationTags.LINEID_TAG];
  let sublineid = substation[stationTags.SUBLINEID_TAG];
  let substationname = substation[stationTags.NAME_TAG];
  let substationid = substation[stationTags.SUBSTATIONID_TAG];
  let ismainline = substation[stationTags.ISMAINLINE_TAG];
  const log = bunyan.createLogger({
    name: substationid,
    level: config.logger.loglevel,
  });
  const topic_name = `interface-${substationid}`;

  const PARAMETERS = parameters.parameters(
    config,
    substation,
    utility,
    tags,
    emitter
  );
  PARAMETERS.initEmitter();
  PARAMETERS.getParameters();

  return {
    substationid: substationid,
    substationname: substationname,
    updateStation(stationinfo: any) {
      substation = stationinfo;
      lineid = substation[stationTags.LINEID_TAG];
      sublineid = substation[stationTags.SUBLINEID_TAG];
      substationname = substation[stationTags.NAME_TAG];
      substationid = substation[stationTags.SUBSTATIONID_TAG];
      ismainline = substation[stationTags.ISMAINLINE_TAG];
      this.substationid = substationid;
      this.substationname = substationname;
    },
    /**
     * Intialize the socket listener on startup of E.A.
     */
    initEmitter() {
      KafkaService.initClient(topic_name);

      /**
       * checkout update Event received from ShopWorx
       */
      emitter.on('checkout', (data) => {
        log.error(`checkout event triggered ${JSON.stringify(data)}`);
        if (data.substationid === substationid) {
          this.checkoutTrigger(data);
        }
      });
      emitter.on('equipmentalarm', (data) => {
        log.error(`equipmentalarm event triggered ${JSON.stringify(data)}`);
        if (data.substationid === substationid) {
          this.toCommitEquipmentAlarm(data);
        }
      });
      emitter.on('stationstatus', (data) => {
        log.error(`stationstatus event triggered ${JSON.stringify(data)}`);
        if (data.substationid === substationid) {
          this.toCommitStationStatus(data);
        }
      });
      emitter.on('cyclerunning', (data) => {
        log.error(`cyclerunning event triggered ${JSON.stringify(data)}`);
        if (data.substationid === substationid) {
          if (data.state == 'In Progress') {
            this.toCommitOrderInProduction(data);
          } else {
            this.toCommitOrderCompleted(data);
          }
        }
      });
      emitter.on('personelcheck', (data) => {
        log.error(
          `personnelqualification event triggered ${JSON.stringify(data)}`
        );
        if (data.substationid === substationid) {
          this.toCommitPersonnelQualification(data);
        }
      });
    },
    checkoutTrigger(data: any) {
      this.toCommitMaterialConsumption(data);
      if (substationid === 'substation-209') {
        this.toCommitOrderInformation(data);
      }
      this.toCommitProductionInspectionData(data);
    },
    async toCommitOrderInProduction(data: any) {
      let payload;
      try {
        let sublinename = '';
        let sublinequery = `query=id=="${data.sublineid}"`;
        const sublinerecord = await elementService.getElementRecords(
          config.elements.subline || 'subline',
          sublinequery
        );
        if (sublinerecord.status === utility.HTTP_STATUS_CODE.SUCCESS) {
          if (
            sublinerecord.data &&
            sublinerecord.data.results &&
            sublinerecord.data.results.length > 0
          ) {
            sublinename = sublinerecord.data.results[0]['name'];
          }
          payload = {
            ProductionLineNo: `${config.stationprefix}${ismainline ? '' : '-'}${
              ismainline ? '' : sublinename
            }`,
            WipOrderNo: data.ordername,
            Equipment: `BW754_${config.stationprefix}${ismainline ? '' : '-'}${
              ismainline ? '' : sublinename
            }_${substationname}`,
            TempParentSerialNo: data.mainid,
            SystemTime: moment(data.starttime).format(
              'YYYY-MM-DD HH:mm:ss.SSS'
            ),
          };
          log.error(`Cyclerunning Start payload ${JSON.stringify(payload)}`);
          const response = await INTERFACE.commitOrderInProduction(payload);
          log.error(`Cyclerunning Start response ${JSON.stringify(response)}`);

          if (response.status === utility.HTTP_STATUS_CODE.SUCCESS) {
            if (
              response.data &&
              response.data.results &&
              response.data.Outputs
            ) {
              log.error(
                `Order In Production Send Successfully for ${substationid}`
              );
            } else {
              log.error(
                `Error for Order In Production Sending  for ${substationid}`
              );
            }
          } else {
            log.error(
              `Error for Order In Production Sending for ${substationname}`
            );
            kafka.sendMessage(topic_name, {
              method: 'commitOrderInProduction',
              payload: payload,
            });
          }
        } else {
          utility.checkSessionExpired(sublinerecord.data);
          this.toCommitOrderInProduction(data);
        }
      } catch (error) {
        log.error(`Error for Order In Production Sending for : ${data.mainid}`);
        if (error.isAxiosError) {
          kafka.sendMessage(topic_name, {
            method: 'commitOrderInProduction',
            payload: payload,
          });
        }
      }
    },
    async toCommitMaterialConsumption(data: any) {
      const batchparameters = this.getParametersByCategory(
        config.defaults.batchID
      );
      let payload;
      try {
        let parameters = [];
        let query = `query=${TAGS.componenttags.MAINID_TAG}=="${data.mainid}"%26%26${TAGS.componenttags.SUBSTATIONID_TAG}=="${substationid}"`;
        query += `%26%26${TAGS.componenttags.PARAMETERCATEGORY_TAG}=="${config.defaults.batchID}"`;
        const response = await elementService.getElementRecords(
          config.elements.component,
          query
        );
        if (response.status === utility.HTTP_STATUS_CODE.SUCCESS) {
          if (
            response.data &&
            response.data.results &&
            response.data.results.length > 0
          ) {
            log.error(`Parameters fetched successfully`);
            parameters = response.data.results;
            // const batchData = this.mapData(parameters, batchparameters);
            const mappingdata = this.mapBatchid(parameters, batchparameters);
            if (
              substationid == 'substation-195' ||
              substationid == 'substation-207'
            ) {
              // add subassemblyid as material
              let componentname = '';
              let asmstation = '';
              if (substationid == 'substation-207') {
                componentname = 'asm_main_hub_welded_lot_code';
                asmstation = 'substation-167';
              } else {
                componentname = 'asm_rotor_carrier_lot_code';
                asmstation = 'substation-215';
              }
              // get subassemblyid
              let asmquery = `query=${TAGS.componenttags.MAINID_TAG}=="${data.mainid}"%26%26${TAGS.componenttags.SUBSTATIONID_TAG}=="${substationid}"`;
              asmquery += `%26%26${TAGS.componenttags.PARAMETERCATEGORY_TAG}=="${config.defaults.subassemblyID}"`;
              const asmresponse = await elementService.getElementRecords(
                config.elements.component,
                asmquery
              );
              let subassemblyid = '';
              let lotcode = '';
              if (asmresponse.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                if (
                  asmresponse.data &&
                  asmresponse.data.results &&
                  asmresponse.data.results.length > 0
                ) {
                  subassemblyid = asmresponse.data.results[0]['componentvalue'];
                  if (subassemblyid) {
                    // get lot code
                    let lotquery = `query=${TAGS.componenttags.MAINID_TAG}=="${subassemblyid}"%26%26${TAGS.componenttags.SUBSTATIONID_TAG}=="${asmstation}"`;
                    query += `%26%26${TAGS.componenttags.PARAMETERCATEGORY_TAG}=="${config.defaults.batchID}"`;
                    query += `%26%26${TAGS.componenttags.COMPONENTNAME_TAG}=="${componentname}"`;
                    const lotresponse = await elementService.getElementRecords(
                      config.elements.component,
                      lotquery
                    );
                    if (
                      lotresponse.status === utility.HTTP_STATUS_CODE.SUCCESS
                    ) {
                      if (
                        lotresponse.data &&
                        lotresponse.data.results &&
                        lotresponse.data.results.length > 0
                      ) {
                        lotcode = lotresponse.data.results[0]['componentvalue'];
                        mappingdata.ComponentCode.push(subassemblyid);
                        mappingdata.LotNo.push(lotcode);
                      }
                    } else {
                      utility.checkSessionExpired(lotresponse.data);
                      this.toCommitMaterialConsumption(data);
                      return;
                    }
                  }
                }
              } else {
                utility.checkSessionExpired(asmresponse.data);
                this.toCommitMaterialConsumption(data);
                return;
              }
            }
            let sublinename = '';
            let sublinequery = `query=id=="${data.sublineid}"`;
            const sublinerecord = await elementService.getElementRecords(
              config.elements.subline || 'subline',
              sublinequery
            );
            if (sublinerecord.status === utility.HTTP_STATUS_CODE.SUCCESS) {
              if (
                sublinerecord.data &&
                sublinerecord.data.results &&
                sublinerecord.data.results.length > 0
              ) {
                sublinename = sublinerecord.data.results[0]['name'];
              }
              payload = {
                ProductionLineNo: `${config.stationprefix}${
                  ismainline ? '' : '-'
                }${ismainline ? '' : sublinename}`,
                WipOrderNo: data.ordername,
                Equipment: `BW754_${config.stationprefix}${
                  ismainline ? '' : '-'
                }${ismainline ? '' : sublinename}_${substationname}`,
                TempParentSerialNo: data.mainid,
                SystemTime: moment(
                  data.createdTimestamp.replace(':', ' ')
                ).format('YYYY-MM-DD HH:mm:ss.SSS'),
                ComponentCode: mappingdata.ComponentCode,
                LotNo: mappingdata.LotNo,
              };
              log.error(
                `MaterialConsumption payload ${JSON.stringify(payload)}`
              );
              const response1 = await INTERFACE.commitMaterialConsumption(
                payload
              );
              log.error(
                `MaterialConsumption response ${JSON.stringify(response1)}`
              );

              if (response1.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                if (
                  response1.data &&
                  response1.data.results &&
                  response1.data.Outputs > 0
                ) {
                  log.error(
                    `Order In Production Send Successfully for ${substationid}`
                  );
                } else {
                  log.error(
                    `Error for Order In Production Sending  for ${substationid}`
                  );
                }
              } else {
                log.error(
                  `Error for Order In Production Sending for ${substationname}`
                );
                kafka.sendMessage(topic_name, {
                  method: 'commitMaterialConsumption',
                  payload: payload,
                });
              }
            } else {
              utility.checkSessionExpired(sublinerecord.data);
              this.toCommitMaterialConsumption(data);
            }
          } else {
            log.error(`Parameters not found  ${JSON.stringify(response.data)}`);
            parameters = [];
          }
        } else {
          log.error(
            `Error in getting data of Parameters for mainid : ${
              data.mainid
            } ${JSON.stringify(response.data)}`
          );
          utility.checkSessionExpired(response.data);
          parameters = [];
          this.toCommitMaterialConsumption(data);
        }
      } catch (error) {
        log.error(`Exception to fetch ComponentId for mainid : ${data.mainid}`);
        const messageObject = error.response ? error.response.data : error;
        log.error(messageObject);
        if (error.isAxiosError) {
          kafka.sendMessage(topic_name, {
            method: 'commitMaterialConsumption',
            payload: payload,
          });
        }
      }
    },
    async toCommitOrderCompleted(data: any) {
      let payload;
      try {
        let sublinename = '';
        let sublinequery = `query=id=="${data.sublineid}"`;
        const sublinerecord = await elementService.getElementRecords(
          config.elements.subline || 'subline',
          sublinequery
        );
        if (sublinerecord.status === utility.HTTP_STATUS_CODE.SUCCESS) {
          if (
            sublinerecord.data &&
            sublinerecord.data.results &&
            sublinerecord.data.results.length > 0
          ) {
            sublinename = sublinerecord.data.results[0]['name'];
          }
          payload = {
            ProductionLineNo: `${config.stationprefix}${ismainline ? '' : '-'}${
              ismainline ? '' : sublinename
            }`,
            WipOrderNo: data.ordername,
            Equipment: `BW754_${config.stationprefix}${ismainline ? '' : '-'}${
              ismainline ? '' : sublinename
            }_${data.substationname}`,
            TempParentSerialNo: data.mainid,
            SystemTime: moment(data.endtime).format('YYYY-MM-DD HH:mm:ss.SSS'),
            Result: data.checkoutresult,
            FinalStep: config.defaults.finalstep || '1',
          };
          log.error(`Cyclerunning End payload ${JSON.stringify(payload)}`);
          const response = await INTERFACE.commitOrderCompleted(payload);
          log.error(`Cyclerunning End response ${JSON.stringify(response)}`);

          if (response.status === utility.HTTP_STATUS_CODE.SUCCESS) {
            if (
              response.data &&
              response.data.results &&
              response.data.Outputs
            ) {
              log.error(
                `Order In Production Send Successfully for ${substationid}`
              );
            } else {
              log.error(
                `Error for Order In Production Sending  for ${substationid}`
              );
            }
          } else {
            log.error(
              `Error for Order In Production Sending for ${substationname}`
            );
            kafka.sendMessage(topic_name, {
              method: 'commitOrderCompleted',
              payload: payload,
            });
          }
        } else {
          utility.checkSessionExpired(sublinerecord.data);
          this.toCommitOrderCompleted(data);
        }
      } catch (error) {
        log.error(
          `Error for Order In Production Sending for ${substationname}`
        );
        if (error.isAxiosError) {
          kafka.sendMessage(topic_name, {
            method: 'commitOrderCompleted',
            payload: payload,
          });
        }
      }
    },
    async toCommitOrderInformation(data: any) {
      let payload;
      try {
        let sublinename = '';
        let sublinequery = `query=id=="${data.sublineid}"`;
        const sublinerecord = await elementService.getElementRecords(
          config.elements.subline || 'subline',
          sublinequery
        );
        if (sublinerecord.status === utility.HTTP_STATUS_CODE.SUCCESS) {
          if (
            sublinerecord.data &&
            sublinerecord.data.results &&
            sublinerecord.data.results.length > 0
          ) {
            sublinename = sublinerecord.data.results[0]['name'];
          }
          payload = {
            ProductionLineNo: `${config.stationprefix}${ismainline ? '' : '-'}${
              ismainline ? '' : sublinename
            }`,
            WipOrderNo: data.ordername,
            Equipment: `BW754_${config.stationprefix}${ismainline ? '' : '-'}${
              ismainline ? '' : sublinename
            }_${substationname}`,
            TempParentSerialNo: data.mainid,
            SystemTime: moment(data.createdTimestamp.replace(':', ' ')).format(
              'YYYY-MM-DD HH:mm:ss.SSS'
            ),
            ParentSerialNo: data.completedproductid || '',
          };
          log.error(`Final Station Report Payload ${JSON.stringify(payload)}`);
          const response = await INTERFACE.commitOrderInformation(payload);
          log.error(
            `Final Station Report response ${JSON.stringify(response)}`
          );
          if (response.status === utility.HTTP_STATUS_CODE.SUCCESS) {
            if (
              response.data &&
              response.data.results &&
              response.data.Outputs
            ) {
              log.error(
                `Order In Production Send Successfully for ${substationid}`
              );
            } else {
              log.error(
                `Error for Order In Production Sending  for ${substationid}`
              );
            }
          } else {
            log.error(
              `Error for Order In Production Sending for ${substationname}`
            );
            kafka.sendMessage(topic_name, {
              method: 'commitOrderInformation',
              payload: payload,
            });
          }
        } else {
          utility.checkSessionExpired(sublinerecord.data);
          this.toCommitOrderInformation(data);
        }
      } catch (error) {
        log.error(
          `Error for Order In Production Sending for ${substationname}`
        );
        if (error.isAxiosError) {
          kafka.sendMessage(topic_name, {
            method: 'commitOrderInformation',
            payload: payload,
          });
        }
      }
    },
    async toCommitProductionInspectionData(data: any) {
      const processparameters = this.getParametersByCategory(
        config.defaults.subprocessparameters
      );
      let payload;
      const elementName = substationid;
      try {
        // check saved parameters
        let parameters = [];
        let query = `query=${TAGS.checkouttags.MAINID_TAG}=="${data.mainid}"%26%26${TAGS.checkouttags.SUBSTATIONID_TAG}=="${substationid}"`;
        const response = await elementService.getElementRecords(
          elementName,
          query
        );
        if (response.status === utility.HTTP_STATUS_CODE.SUCCESS) {
          if (
            response.data &&
            response.data.results &&
            response.data.results.length > 0
          ) {
            log.error(`Parameters fetched successfully`);
            parameters = response.data.results[0];
            const processData = this.mapData(parameters, processparameters);
            const mappingdata = this.mapPayload(
              processData,
              config.defaults.mappingway
            );
            let sublinename = '';
            let sublinequery = `query=id=="${data.sublineid}"`;
            const sublinerecord = await elementService.getElementRecords(
              config.elements.subline || 'subline',
              sublinequery
            );
            if (sublinerecord.status === utility.HTTP_STATUS_CODE.SUCCESS) {
              if (
                sublinerecord.data &&
                sublinerecord.data.results &&
                sublinerecord.data.results.length > 0
              ) {
                sublinename = sublinerecord.data.results[0]['name'];
              }
              payload = {
                ProductionLineNo: `${config.stationprefix}${
                  ismainline ? '' : '-'
                }${ismainline ? '' : sublinename}`,
                WipOrderNo: data.ordername,
                Equipment: `BW754_${config.stationprefix}${
                  ismainline ? '' : '-'
                }${ismainline ? '' : sublinename}_${substationname}`,
                TempParentSerialNo: data.mainid,
                SystemTime: moment().format('YYYY-MM-DD HH:mm:ss.SSS'),
                CharacteristicCode: mappingdata.CharacteristicCode,
                CharacteristicVarValue: mappingdata.CharacteristicVarValue,
                CharacteristicAttValue: mappingdata.CharacteristicAttValue,
                DefectCode: mappingdata.DefectCode,
                CharacteristicResult: mappingdata.CharacteristicResult,
                WipOperStep: mappingdata.WipOperStep,
              };
              log.error(
                `ProductionInspectionData Payload ${JSON.stringify(payload)}`
              );
              const response1 = await INTERFACE.commitProductionInspectionData(
                payload
              );
              log.error(
                `ProductionInspectionData response ${JSON.stringify(response1)}`
              );

              if (response1.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                if (
                  response1.data &&
                  response1.data.results &&
                  response1.data.Outputs
                ) {
                  log.error(`Parameters Send Successfully for ${substationid}`);
                } else {
                  log.error(
                    `Error for Order In Parameters Sending  for ${substationid}`
                  );
                }
              } else {
                log.error(
                  `Error for Order In Parameters Sending for ${substationname}`
                );
                kafka.sendMessage(topic_name, {
                  method: 'commitProductionInspectionData',
                  payload: payload,
                });
              }
            } else {
              utility.checkSessionExpired(sublinerecord.data);
              this.toCommitProductionInspectionData(data);
            }
          } else {
            log.error(`Parameters not found  ${JSON.stringify(response.data)}`);
            parameters = [];
          }
        } else {
          log.error(
            `Error in getting data of Parameters for mainid : ${
              data.mainid
            } ${JSON.stringify(response.data)}`
          );
          utility.checkSessionExpired(response.data);
          parameters = [];
          this.toCommitProductionInspectionData(data);
        }
      } catch (error) {
        log.error(`Exception to fetch Parameters for mainid : ${data.mainid}`);
        const messageObject = error.response ? error.response.data : error;
        log.error(messageObject);
        if (error.isAxiosError) {
          kafka.sendMessage(topic_name, {
            method: 'commitProductionInspectionData',
            payload: payload,
          });
        }
      }
    },
    async toCommitEquipmentAlarm(data: any) {
      let payload;
      try {
        payload = {
          ProductionLineNo: `${config.stationprefix}${ismainline ? '' : '-'}${
            ismainline ? '' : data.sublinename
          }`,
          Equipment: `BW754_${config.stationprefix}${ismainline ? '' : '-'}${
            ismainline ? '' : data.sublinename
          }_${data.substationname}`,
          ReasonCode: data.reasoncode,
          AlarmType: data.alarmType || 1,
          AlarmLevel: data.alarmLevel || 1,
          SystemTime: moment(data.starttime).format('YYYY-MM-DD HH:mm:ss.SSS'),
        };
        log.error(`EquipmentAlarm Payload ${JSON.stringify(payload)}`);
        const response = await INTERFACE.commitEquipmentAlarm(payload);
        log.error(`EquipmentAlarm response ${JSON.stringify(response)}`);

        if (response.status === utility.HTTP_STATUS_CODE.SUCCESS) {
          if (response.data && response.data.results && response.data.Outputs) {
            log.error(`Equipment Alarm Send Successfully for ${substationid}`);
          } else {
            log.error(`Error for Equipment Alarm Sending  for ${substationid}`);
          }
        } else {
          log.error(`Error for Equipment Alarm Sending for ${substationname}`);
          kafka.sendMessage(topic_name, {
            method: 'commitEquipmentAlarm',
            payload: payload,
          });
        }
      } catch (error) {
        log.error(`Error for Equipment Alarm Sending for : ${data.mainid}`);
        const messageObject = error.response ? error.response.data : error;
        log.error(messageObject);
        if (error.isAxiosError) {
          kafka.sendMessage(topic_name, {
            method: 'commitEquipmentAlarm',
            payload: payload,
          });
        }
      }
    },
    async toCommitStationStatus(data: any) {
      let payload;
      try {
        payload = {
          ProductionLineNo: `${config.stationprefix}${ismainline ? '' : '-'}${
            ismainline ? '' : data.sublinename
          }`,
          ReasonCode: data.status,
          Equipment: `BW754_${config.stationprefix}${ismainline ? '' : '-'}${
            ismainline ? '' : data.sublinename
          }_${data.substationname}`,
          SystemTime: moment(data.starttime).format('YYYY-MM-DD HH:mm:ss.SSS'),
        };
        log.error(`EquipmentStatus Payload ${JSON.stringify(payload)}`);
        const response = await INTERFACE.changeEquipmentStatus(payload);
        log.error(`EquipmentStatus response ${JSON.stringify(response)}`);

        if (response.status === utility.HTTP_STATUS_CODE.SUCCESS) {
          if (response.data && response.data.results && response.data.Outputs) {
            log.error(`StationStatus Send Successfully for ${substationname}`);
          } else {
            log.error(`Error for StationStatus Sending  for ${substationname}`);
            kafka.sendMessage(topic_name, {
              method: 'changeEquipmentStatus',
              payload: payload,
            });
          }
        } else {
          log.error(`Error for StationStatus Sending for ${substationname}`);
          kafka.sendMessage(topic_name, {
            method: 'changeEquipmentStatus',
            payload: payload,
          });
        }
      } catch (error) {
        log.error(`Error for StationStatus Sending for : ${data.mainid}`);
        const messageObject = error.response ? error.response.data : error;
        log.error(messageObject);
        if (error.isAxiosError) {
          kafka.sendMessage(topic_name, {
            method: 'changeEquipmentStatus',
            payload: payload,
          });
        }
      }
    },
    async toCommitPersonnelQualification(data: any) {
      let payload;
      try {
        payload = {
          EmployeeNo: data.personelcheckresult,
          WipOrderNo: data.ordername,
          Equipment: `BW754_${config.stationprefix}${ismainline ? '' : '-'}${
            ismainline ? '' : data.sublinename
          }_${data.substationname}`,
          SystemTime: moment(data.createdTimestamp.replace(':', ' ')).format(
            'YYYY-MM-DD HH:mm:ss.SSS'
          ),
        };
        log.error(`PersonnelQualification Payload ${JSON.stringify(payload)}`);
        const response = await INTERFACE.checkPersonnelQualification(payload);

        log.error(
          `PersonnelQualification response ${JSON.stringify(response)}`
        );
        if (response.status === utility.HTTP_STATUS_CODE.SUCCESS) {
          if (response.data && response.data.results && response.data.Outputs) {
            log.error(
              `PersonnelQualification Send Successfully for ${substationid}`
            );
          } else {
            log.error(
              `Error for PersonnelQualification Sending  for ${substationid}`
            );
          }
        } else {
          log.error(
            `Error for PersonnelQualification Sending for ${substationname}`
          );
          kafka.sendMessage(topic_name, {
            method: 'checkPersonnelQualification',
            payload: payload,
          });
        }
      } catch (error) {
        log.error(
          `Error for PersonnelQualification Sending for : ${data.mainid}`
        );
        const messageObject = error.response ? error.response.data : error;
        log.error(messageObject);
        if (error.isAxiosError) {
          kafka.sendMessage(topic_name, {
            method: 'checkPersonnelQualification',
            payload: payload,
          });
        }
      }
    },
    getParametersByCategory(category: string) {
      const parameters: any[] = [];
      PARAMETERS.parametersList.filter((item) => {
        if (item[parameterTags.PARAMETERCATEGORY_TAG] === category) {
          parameters.push(item);
        }
      });
      return parameters;
    },
    mapData(data: any, parameters: any) {
      const mapData: any = {};
      parameters.forEach((element: any) => {
        const key = element.name;
        if (_.has(data, key)) {
          mapData[key] = data[key];
        }
      });

      return mapData;
    },
    getParameterName(data: any, str: string) {
      const parameters: any[] = [];
      for (const key in data) {
        if (Object.prototype.hasOwnProperty.call(data, key)) {
          const keyarr: string[] = key.split('_');
          if (keyarr.length >= 2) {
            if (keyarr[keyarr.length - 1] == str) {
              keyarr.pop();
              parameters.push(keyarr.join('_'));
            }
          }
        }
      }

      return parameters;
    },
    mapPayload(
      data: any,
      type: number
    ): {
      CharacteristicCode: any[];
      CharacteristicVarValue: any[];
      CharacteristicAttValue: any[];
      DefectCode: any[];
      CharacteristicResult: any[];
      WipOperStep: any[];
    } {
      let mappingdata: {
        CharacteristicCode: any[];
        CharacteristicVarValue: any[];
        CharacteristicAttValue: any[];
        DefectCode: any[];
        CharacteristicResult: any[];
        WipOperStep: any[];
      } = {
        CharacteristicCode: [],
        CharacteristicVarValue: [],
        CharacteristicAttValue: [],
        DefectCode: [],
        CharacteristicResult: [],
        WipOperStep: [],
      };

      if (type == 1) {
        let parameters = this.getParameterName(data, 'value');
        // normal
        parameters.forEach((item: string) => {
          mappingdata.CharacteristicCode.push(item.trim());
          mappingdata.DefectCode.push(data[`${item}_ng_code`] || '');
          mappingdata.CharacteristicResult.push(data[`${item}_result`] || 0);
          mappingdata.WipOperStep.push('1');
          if (data[`${item}_value`]) {
            if (
              typeof data[`${item}_value`] === 'number' &&
              !Number.isNaN(data[`${item}_value`])
            ) {
              mappingdata.CharacteristicVarValue.push(
                data[`${item}_value`].toString()
              );
              mappingdata.CharacteristicAttValue.push('');
            } else {
              mappingdata.CharacteristicVarValue.push('');
              mappingdata.CharacteristicAttValue.push(
                data[`${item}_value`].toString()
              );
            }
          } else {
            mappingdata.CharacteristicVarValue.push('');
            mappingdata.CharacteristicAttValue.push('');
          }
        });
      } else if (type == 2) {
        let parameters = this.getParameterName(data, 'result');
        // screwing
        parameters.forEach((item: string) => {
          // _torque
          mappingdata.CharacteristicCode.push(`${item.trim()}_torque`);
          mappingdata.CharacteristicVarValue.push(
            data[`${item}_torque`].toString()
          );
          mappingdata.CharacteristicAttValue.push('');
          mappingdata.DefectCode.push(data[`${item}_ng_code`] || '');
          mappingdata.CharacteristicResult.push(data[`${item}_result`]);
          mappingdata.WipOperStep.push('1');
          // _angle
          mappingdata.CharacteristicCode.push(`${item.trim()}_angle`);
          mappingdata.CharacteristicVarValue.push(
            data[`${item}_angle`].toString()
          );
          mappingdata.CharacteristicAttValue.push('');
          mappingdata.DefectCode.push(data[`${item}_ng_code`] || '');
          mappingdata.CharacteristicResult.push(data[`${item}_result`]);
          mappingdata.WipOperStep.push('1');
          // _counter
          mappingdata.CharacteristicCode.push(`${item.trim()}_counter`);
          mappingdata.CharacteristicVarValue.push(
            data[`${item}_counter`].toString()
          );
          mappingdata.CharacteristicAttValue.push('');
          mappingdata.DefectCode.push(data[`${item}_ng_code`] || '');
          mappingdata.CharacteristicResult.push(data[`${item}_result`]);
          mappingdata.WipOperStep.push('1');
        });
      } else if (type == 3) {
        let parameters = this.getParameterName(data, 'value');
        parameters.forEach((item: string) => {
          mappingdata.CharacteristicCode.push(item.trim());
          mappingdata.DefectCode.push(data[`${item}_ng_code`] || '');
          mappingdata.CharacteristicResult.push(data[`${item}_result`] || 0);
          mappingdata.WipOperStep.push('1');
          mappingdata.CharacteristicVarValue.push(
            data[`${item}_value`].toString()
          );
          mappingdata.CharacteristicAttValue.push('');
        });
      }

      return mappingdata;
    },
    mapBatchid(
      data: any[],
      parameters: any[]
    ): {
      ComponentCode: string[];
      LotNo: string[];
    } {
      let mappingdata: {
        ComponentCode: string[];
        LotNo: string[];
      } = {
        ComponentCode: [],
        LotNo: [],
      };
      let parameternames = parameters
        .filter((item: any) => {
          return item.name.includes('material_id');
        })
        .map((item: any) => {
          return item.name.replace('_material_id').trim();
        });
      parameternames.forEach((item: any) => {
        let componentcode = `${item}_material_id`;
        let lotcode = `${item}_lot_code`;
        let componentcodevalue = data.find((item: any) => {
          return item[tags.componenttags.COMPONENTNAME_TAG] == componentcode;
        });
        let lotcodevalue = data.find((item: any) => {
          return item[tags.componenttags.COMPONENTNAME_TAG] == lotcode;
        });
        if (componentcodevalue) {
          mappingdata.ComponentCode.push(
            componentcodevalue[tags.componenttags.COMPONENTVALUE_TAG]
          );
        } else {
          mappingdata.ComponentCode.push('');
        }
        if (lotcodevalue) {
          mappingdata.LotNo.push(
            componentcodevalue[tags.componenttags.COMPONENTVALUE_TAG]
          );
        } else {
          mappingdata.LotNo.push('');
        }
      });
      return mappingdata;
    },
  };
}
export default { stations };
