'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketListenerJS = void 0;
const tslib_1 = require("tslib");
const bunyan_1 = tslib_1.__importDefault(require("bunyan"));
const events_1 = tslib_1.__importDefault(require("events"));
const socket_io_client_1 = tslib_1.__importDefault(require("socket.io-client"));
const config_json_1 = tslib_1.__importDefault(require("../config/config.json"));
let emitter = new events_1.default.EventEmitter();
function socketlistener(config = config_json_1.default) {
    let log = bunyan_1.default.createLogger({
        name: 'Socket Listener',
        level: config.logger.loglevel,
    });
    let socketio = config.socketio;
    let server = config.server;
    return {
        init: function () {
            log.info(`${socketio.host} : ${socketio.port}`);
            let defaultSocket = socket_io_client_1.default(`${server.protocol}://${socketio.host}:${socketio.port}`);
            defaultSocket.on('connect', () => {
                log.info(`Default update_<elementName> Socket connected `);
            });
            defaultSocket.on('disconnect', () => {
                log.error(`Default update_<elementName> Socket disconnected `);
            });
            defaultSocket.on('update_recipe', (data) => {
                emitter.emit('recipe', data);
            });
            defaultSocket.on('update_order', (data) => {
                emitter.emit('order', data);
            });
            defaultSocket.on('update_substation', (data) => {
                emitter.emit('substation', data);
            });
            defaultSocket.on('update_parameters', (data) => {
                emitter.emit('parameters', data);
            });
            defaultSocket.on('update_stationstatus', (data) => {
                emitter.emit('stationstatus', data);
            });
            defaultSocket.on('update_cyclerunning', (data) => {
                emitter.emit('cyclerunning', data);
            });
            defaultSocket.on('update_checkout', (data) => {
                emitter.emit('checkout', data);
            });
            defaultSocket.on('update_personelcheck', (data) => {
                emitter.emit('personelcheck', data);
            });
            if (socketio.namespace) {
                this.socketNamespace();
            }
        },
        socketNamespace: function () {
            let namespace = socketio.namespace;
            let eventName = socketio.eventname;
            let socketNamespace = socket_io_client_1.default(`${server.protocol}://${socketio.host}:${socketio.port}/${namespace}`);
            socketNamespace.on('connect', () => {
                emitter.emit('connect', 'connect');
                log.info(`SWX analyzer_<eventname> Socket connected `);
            });
            socketNamespace.on('disconnect', () => {
                log.error(`SWX analyzer_<eventname> Socket disconnected `);
            });
            socketNamespace.on(`${namespace}_${eventName}`, (data) => {
                emitter.emit(`${eventName}`, data);
            });
            socketNamespace.on(`${namespace}_${eventName}plcerror`, (error) => {
                emitter.emit(`${eventName}plcerror`, error);
            });
            socketNamespace.on(`${namespace}_recipedownload`, (data) => {
                emitter.emit('recipedownload', data);
            });
            socketNamespace.on(`${namespace}_recipeupload`, (data) => {
                emitter.emit('recipeupload', data);
            });
            socketNamespace.on(`${namespace}_parameterupload`, (data) => {
                emitter.emit('parameterupload', data);
            });
        },
    };
}
exports.SocketListenerJS = {
    emitter,
    socketlistener,
};
