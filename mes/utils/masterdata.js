'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
exports.MasterDataJS = void 0;
const tslib_1 = require("tslib");
const element_service_1 = tslib_1.__importDefault(require("../service/element.service"));
const bunyan_1 = tslib_1.__importDefault(require("bunyan"));
const config_json_1 = tslib_1.__importDefault(require("../config/config.json"));
const Utility = tslib_1.__importStar(require("./utility"));
function masterdata(config = config_json_1.default, utility = Utility, tags) {
    const log = bunyan_1.default.createLogger({
        name: 'Master Data',
        level: config.logger.loglevel,
    });
    const elements = config.elements;
    const defaults = config.defaults;
    const parameterTags = tags.parametertags;
    const substationTags = tags.substationtags;
    let retryServerTimer = defaults.retryServerTimer || 10;
    return {
        stationInfo: [],
        getStationInformation() {
            return tslib_1.__awaiter(this, void 0, void 0, function* () {
                const elementName = elements.substation || 'substation';
                try {
                    let query = `query=${[substationTags.SUBSTATIONID_TAG]}=="${config.substationid}"`;
                    query += `&sortquery=createdTimestamp==-1&pagenumber=1&pagesize=1`;
                    const response = yield element_service_1.default.getElementRecords(elementName, query);
                    if (response.data &&
                        response.data.results &&
                        response.data.results.length) {
                        log.error(`station fetched successfully`);
                        this.stationInfo = response.data.results;
                    }
                    else {
                        log.error(`station not found for elementName : ${elementName} ${JSON.stringify(response.data)}`);
                        utility.checkSessionExpired(response.data);
                        yield utility.setTimer(retryServerTimer);
                        yield this.getStationInformation();
                    }
                }
                catch (ex) {
                    log.error(`Exception to fetch recipe for element : ${elementName}`);
                    const messageObject = ex.response ? ex.response.data : ex;
                    log.error(messageObject);
                    yield utility.setTimer(retryServerTimer);
                    yield this.getStationInformation();
                }
            });
        },
    };
}
exports.MasterDataJS = {
    masterdata,
};
