"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.setTimer = exports.cloneDeep = exports.arrayUniqueByKey = exports.checkSessionExpired = exports.assignDataToSchema = exports.throwError = exports.validateConfigfileParameters = exports.checkIfExists = exports.concat = exports.requestTimeout = exports.HTTP_STATUS_CODE = exports.DATA_TYPE = exports.emitter = void 0;
const tslib_1 = require("tslib");
const lodash_1 = tslib_1.__importDefault(require("lodash"));
const bunyan_1 = tslib_1.__importDefault(require("bunyan"));
const events_1 = tslib_1.__importDefault(require("events"));
const config_json_1 = tslib_1.__importDefault(require("../config/config.json"));
exports.emitter = new events_1.default.EventEmitter();
const log = bunyan_1.default.createLogger({ name: 'utility', level: 20 });
exports.DATA_TYPE = {
    STRING: 'string',
    NUMBER: 'number',
    BOOLEAN: 'boolean',
    OBJECT: 'object',
};
exports.HTTP_STATUS_CODE = {
    SUCCESS: 200,
    ACCEPTED: 202,
    BAD_REQUEST: 406,
    NOT_ACCEPTABLE: 406,
    INTERNAL_SERVER_ERROR: 500,
};
exports.requestTimeout = {
    timeout: 45 * 1000,
};
exports.concat = (...strings) => {
    return lodash_1.default.reduce(strings, (accumulator, currentItem) => {
        return accumulator + currentItem;
    });
};
exports.checkIfExists = (configParam, configParamString, dataType) => {
    if (typeof configParam != 'boolean' && !configParam) {
        log.fatal('Configuration parameter is invalid OR absent: ' + configParamString);
        process.exit(1);
    }
    if (typeof configParam != dataType) {
        log.fatal("Data type for configuration parameter '" +
            configParamString +
            "' must be: " +
            dataType);
        process.exit(1);
    }
};
exports.validateConfigfileParameters = (CONFIG = config_json_1.default) => {
    log.info('Validating Configuration file.');
    exports.checkIfExists(CONFIG.industryid, 'CONFIG.industryid', exports.DATA_TYPE.NUMBER);
    exports.checkIfExists(CONFIG.loginType, 'CONFIG.loginType', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.server, 'CONFIG.server', exports.DATA_TYPE.OBJECT);
    exports.checkIfExists(CONFIG.server.protocol, 'CONFIG.server.protocol', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.server.host, 'CONFIG.server.host', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.server.port, 'CONFIG.server.port', exports.DATA_TYPE.NUMBER);
    exports.checkIfExists(CONFIG.socketio, 'CONFIG.socketio', exports.DATA_TYPE.OBJECT);
    exports.checkIfExists(CONFIG.socketio.host, 'CONFIG.socketio.host', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.socketio.port, 'CONFIG.socketio.port', exports.DATA_TYPE.NUMBER);
    exports.checkIfExists(CONFIG.socketio.namespace, 'CONFIG.socketio.namespace', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.socketio.eventname, 'CONFIG.socketio.eventname', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.credential, 'CONFIG.credential', exports.DATA_TYPE.OBJECT);
    exports.checkIfExists(CONFIG.credential.password, 'CONFIG.credential.password', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.credential.identifier, 'CONFIG.credential.identifier', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.kafkaEdge, 'CONFIG.kafkaEdge', exports.DATA_TYPE.OBJECT);
    exports.checkIfExists(CONFIG.kafkaEdge.host, 'CONFIG.kafkaEdge.host', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.kafkaEdge.port, 'CONFIG.kafkaEdge.port', exports.DATA_TYPE.NUMBER);
    exports.checkIfExists(CONFIG.kafkaEdge.autoCommit, 'CONFIG.kafkaEdge.autoCommit', exports.DATA_TYPE.BOOLEAN);
    exports.checkIfExists(CONFIG.kafkaEdge.fetchMinBytes, 'CONFIG.kafkaEdge.fetchMinBytes', exports.DATA_TYPE.NUMBER);
    exports.checkIfExists(CONFIG.kafkaEdge.fetchMaxBytes, 'CONFIG.kafkaEdge.fetchMaxBytes', exports.DATA_TYPE.NUMBER);
    exports.checkIfExists(CONFIG.logger, 'CONFIG.logger', exports.DATA_TYPE.OBJECT);
    exports.checkIfExists(CONFIG.logger.loglevel, 'CONFIG.logger.loglevel', exports.DATA_TYPE.NUMBER);
    exports.checkIfExists(CONFIG.elements, 'CONFIG.elements', exports.DATA_TYPE.OBJECT);
    exports.checkIfExists(CONFIG.elements.substation, 'CONFIG.elements.substation', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.elements.order, 'CONFIG.elements.order', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.elements.orderproduct, 'CONFIG.elements.orderproduct', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.elements.orderrecipe, 'CONFIG.elements.orderrecipe', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.elements.orderroadmap, 'CONFIG.elements.orderroadmap', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.elements.checkin, 'CONFIG.elements.checkin', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.elements.checkout, 'CONFIG.elements.checkout', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.elements.component, 'CONFIG.elements.component', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.elements.partstatus, 'CONFIG.elements.partstatus', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.elements.rework, 'CONFIG.elements.rework', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.elements.bomdetails, 'CONFIG.elements.bomdetails', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.elements.componentcheck, 'CONFIG.elements.componentcheck', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.elements.productionimage, 'CONFIG.elements.productionimage', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.elements.productionimageinfo, 'CONFIG.elements.productionimageinfo', exports.DATA_TYPE.STRING);
    exports.checkIfExists(CONFIG.defaults, 'CONFIG.defaults', exports.DATA_TYPE.OBJECT);
    exports.checkIfExists(CONFIG.defaults.isPreviousDataIgnored, 'CONFIG.defaults.isPreviousDataIgnored', exports.DATA_TYPE.BOOLEAN);
    exports.checkIfExists(CONFIG.status, 'CONFIG.status', exports.DATA_TYPE.OBJECT);
    exports.checkIfExists(CONFIG.status.running, 'CONFIG.status.running', exports.DATA_TYPE.STRING);
    log.info('Configuration file successfully validated.');
};
exports.throwError = (reply, err) => {
    reply.code(500).send({ message: err });
};
exports.assignDataToSchema = function (data, fields) {
    let postData = {};
    let prefix = '';
    if (fields.length > 0) {
        for (let i = 0; fields && i < fields.length; i++) {
            let tagname = prefix + fields[i].tagName;
            let choice = fields[i].emgTagType || '';
            switch (choice) {
                case 'Int':
                case 'Double':
                case 'Float':
                case 'Long':
                    if (data[tagname] || data[tagname] === 0) {
                        postData[fields[i].tagName] =
                            data[tagname] || data[tagname] === 0
                                ? +data[tagname]
                                : data[tagname];
                    }
                    break;
                case 'String':
                    if (data[tagname] || data[tagname] === '') {
                        let stringValue = data[tagname]
                            ? data[tagname].toString()
                            : data[tagname];
                        stringValue = stringValue
                            ? stringValue.replace(/\u0000/g, '')
                            : stringValue;
                        postData[fields[i].tagName] = stringValue
                            ? stringValue.trim()
                            : stringValue;
                    }
                    break;
                case 'Boolean':
                    postData[fields[i].tagName] = data[tagname] || false;
                    break;
                default:
                    if (data[tagname] || data[tagname] == 0) {
                        postData[fields[i].tagName] = data[tagname];
                    }
            }
        }
    }
    else {
        postData = data;
    }
    return postData;
};
exports.checkSessionExpired = (data) => {
    if (data && data.errors && data.errors.errorCode === 'INVALID_SESSION') {
        log.error(`Session Id expired ${JSON.stringify(data.errors)}`);
        exports.emitter.emit('sessionExpired');
    }
};
exports.arrayUniqueByKey = (data, key) => {
    const unique = [
        ...new Map(data.map((item) => [item[key], item])).values(),
    ];
    return unique;
};
exports.cloneDeep = (data) => {
    return JSON.parse(JSON.stringify(data));
};
function setTimer(time) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        time = time || 1;
        yield new Promise((resolve) => setTimeout(resolve, time * 1000));
    });
}
exports.setTimer = setTimer;
