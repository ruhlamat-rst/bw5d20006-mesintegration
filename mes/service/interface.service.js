"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const axios_1 = tslib_1.__importDefault(require("axios"));
const config_json_1 = tslib_1.__importDefault(require("../config/config.json"));
const URL = `${config_json_1.default.messerver.protocol}://${config_json_1.default.messerver.host}/Apriso/httpServices/operations`;
class InterfaceService {
    constructor() { }
    handshake() {
        return axios_1.default.get(`${URL}/Handshack`);
    }
    checkPersonnelQualification(payload) {
        const param = {
            Inputs: payload,
        };
        debugger;
        return axios_1.default.post(`${URL}/PersonnelQualificationVerify`, param);
    }
    changeEquipmentStatus(payload) {
        const param = {
            Inputs: payload,
        };
        return axios_1.default.post(`${URL}/EquipmentStatusChange`, param);
    }
    commitOrderInProduction(payload) {
        const param = {
            Inputs: payload,
        };
        return axios_1.default.post(`${URL}/WorkstationStartInformation`, param);
    }
    commitMaterialConsumption(payload) {
        const param = {
            Inputs: payload,
        };
        return axios_1.default.post(`${URL}/MaterialConsumption`, param);
    }
    commitOrderCompleted(payload) {
        const param = {
            Inputs: payload,
        };
        return axios_1.default.post(`${URL}/WorkstationCompleteInformation`, param);
    }
    commitOrderInformation(payload) {
        const param = {
            Inputs: payload,
        };
        return axios_1.default.post(`${URL}/ProductionCompleteReport`, param);
    }
    commitProductionInspectionData(payload) {
        const param = {
            Inputs: payload,
        };
        return axios_1.default.post(`${URL}/ProductionInspectionDataUpload`, param);
    }
    commitEquipmentAlarm(payload) {
        const param = {
            Inputs: payload,
        };
        return axios_1.default.post(`${URL}/EquipmentAlarm`, param);
    }
}
exports.default = new InterfaceService();
