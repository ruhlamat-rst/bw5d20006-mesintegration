"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const kafka_node_1 = tslib_1.__importDefault(require("kafka-node"));
const interface_service_1 = tslib_1.__importDefault(require("./interface.service"));
const UTILITY = tslib_1.__importStar(require("../utils/utility"));
const config_json_1 = tslib_1.__importDefault(require("../config/config.json"));
const bunyan_1 = tslib_1.__importDefault(require("bunyan"));
const log = bunyan_1.default.createLogger({
    name: 'kafka',
    level: config_json_1.default.logger.loglevel || 20,
});
const retryServerTimer = config_json_1.default.defaults.retryServerTimer;
const maxServerRetryCount = config_json_1.default.defaults.maxServerRetryCount;
const kafkaConfig = config_json_1.default.kafkaEdge;
let fixedString = '';
for (var i = 0; i < kafkaConfig.fetchMaxBytes; i++) {
    fixedString += '-';
}
class KafkaService {
    constructor() { }
    initClient(topic_name) {
        this.kafkaClient = new kafka_node_1.default.KafkaClient({
            kafkaHost: `${kafkaConfig.host}:${kafkaConfig.port}`,
        });
        this.kafkaClient.on('error', (error) => {
            log.error(`client error: ${error}`);
            this.initClient(topic_name);
        });
        this.initProducer();
        this.initConsumer(topic_name);
    }
    initProducer() {
        if (!this.kafkaClient)
            return;
        this.producer = new kafka_node_1.default.Producer(this.kafkaClient);
        this.producer.on(`ready`, () => {
            log.error(`Producer Ready`);
        });
        this.producer.on(`error`, (err) => {
            log.error(`[kafka-producer]: connection errored: ${err}`);
        });
    }
    initConsumer(topic_name) {
        if (!this.kafkaClient)
            return;
        let offset = new kafka_node_1.default.Offset(this.kafkaClient);
        let latest = 1;
        offset.fetchLatestOffsets([topic_name], function (err, offsets) {
            if (err) {
                log.error(`error fetching latest offsets from kafka topic`);
                log.error(`${err}`);
                return;
            }

            Object.keys(offsets[topic_name]).forEach((o) => {
                latest =
                    offsets[topic_name][o] > latest ? offsets[topic_name][o] : latest;
            });
        });

        this.consumer = new kafka_node_1.default.ConsumerGroup({
            kafkaHost: `${kafkaConfig.host}:${kafkaConfig.port}`,
            groupId: topic_name,
            sessionTimeout: 15000,
            protocol: ['roundrobin'],
            encoding: 'utf8',
            fromOffset: kafkaConfig.fromOffset,
            outOfRangeOffset: kafkaConfig.outOfRangeOffset,
            autoCommit: false,
            autoCommitIntervalMs: 500,
            heartbeatInterval: 100,
            maxTickMessages: 1,
        }, topic_name);
        this.consumer.on('connect', () => {
            // log.info('kafka consumerGroup connect');
        });
        this.consumer.on('offsetOutOfRange', function (error) {
             var _a;
            log.error(`offsetOutOfRange ${error}`);
             (_a = this.consumer) === null || _a === void 0 ? void 0 : _a.close(true, (err, res = {}) => {
                if (!err) {
                    log.error(`kafka plan update event consumer connection closed successfully ${res}`);
                }
                else {
                    log.error(`Error in closing kafka plan update event consumer ${err}`);
                }
                this.initConsumer(topic_name);
            });
        });
        this.consumer.on('error', (error) => {
            var _a;
            log.error(`Error in kafka consumer: ${error}`);
            (_a = this.consumer) === null || _a === void 0 ? void 0 : _a.close(true, (err, res = {}) => {
                if (!err) {
                    log.error(`kafka plan update event consumer connection closed successfully ${res}`);
                }
                else {
                    log.error(`Error in closing kafka plan update event consumer ${err}`);
                }
                this.initConsumer(topic_name);
            });
        });
        this.consumer.on(`message`, (message) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            const payload = JSON.parse(message.value);
            delete payload.ignore;
            if (config_json_1.default.defaults.isPreviousDataIgnored) {
                if (message && message.offset && message.offset >= latest - 1) {
                      this.repostCommit(payload)
                }
            }
            else {
                      this.repostCommit(payload)
            }
        }));
    }
    sendMessage(topic_name, data) {
        try {
            if (this.producer && this.producer.ready) {
                data.ignore = this.appendChar(data);
                const payload = [
                    {
                        topic: topic_name,
                        messages: JSON.stringify(data),
                    },
                ];
                this.producer.send(payload, (err, data) => {
                    if (err) {
                        log.error(`Producer Error sending data for ${topic_name} ${err}`);
                    }
                    else {
                        log.info(`[kafka-producer -> ${topic_name}] size : ${JSON.stringify(payload).length}, payload : ${JSON.stringify(payload)} broker update success`);
                    }
                });
            }
            else {
                log.error(`Kafka Producer is not ready. Please check kafka is configuration and kafka is running or not`);
            }
        }
        catch (ex) {
            log.error(`exeption in writing data to kafka topic ${ex}`);
        }
    }
    appendChar(data) {
        try {
            const lengthOfStr = JSON.stringify(data).length;
            const subString = fixedString.substring(lengthOfStr + 12, fixedString.length);
            return subString;
        }
        catch (ex) {
            log.error(`Exception in checking length of object ${ex}`);
        }
    }
    repostCommit(data, counter) {
        var _a;
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            log.error(`repostCommit Payload  ${JSON.stringify(data)}`);
            try {
                const response = yield interface_service_1.default[data.method](data.payload);
                if (response.status === UTILITY.HTTP_STATUS_CODE.SUCCESS) {
                     (_a = this.consumer) === null || _a === void 0 ? void 0 : _a.commit(() => {
                        log.error(`next commit`);
                    });
                    log.error(`repostCommit successfully`);
                }
                else {
                    yield UTILITY.setTimer(retryServerTimer);
                }
            }
            catch (error) {
                yield UTILITY.setTimer(retryServerTimer);
                yield this.repostCommit(data);
            }
        });
    }
}
exports.default = new KafkaService();
