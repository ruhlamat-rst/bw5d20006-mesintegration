[Unit]
Description=This Mes Integration service used to post data to the MES
After=network.target

[Service]
ExecStart=/bin/sh -c 'exec node {{ nodebots_dir}}mes/index.js {{item}} >> {{ nodebots_dir}}logs/mes-{{item}}.log'
Restart=on-failure
User=emgda
Group=emgda

[Install]
WantedBy=multi-user.target