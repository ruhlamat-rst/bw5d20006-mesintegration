'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const tags_json_1 = tslib_1.__importDefault(require("./tags/tags.json"));
const UTILITY = tslib_1.__importStar(require("./utils/utility"));
const auth_1 = require("./utils/auth");
const servertime_1 = require("./utils/servertime");
const masterdata_1 = require("./utils/masterdata");
const socketlistener_1 = require("./utils/socketlistener");
const kafka_service_1 = tslib_1.__importDefault(require("./service/kafka.service"));
const station_1 = tslib_1.__importDefault(require("./src/station"));
const handshake_1 = tslib_1.__importDefault(require("./src/handshake"));
const bunyan_1 = tslib_1.__importDefault(require("bunyan"));
let CONFIG, configfilename = process.argv[2];

try {
    if (!configfilename) {
        console.log(`Error please pass correct argument of config file name`);
        process.exit(1);
    }
    let configfilepath = `./config/${configfilename}`;
    CONFIG = require(configfilepath);
}
catch (ex) {
    console.log(`Exception in reading config file ${ex}`);
    process.exit(1);
}
UTILITY.validateConfigfileParameters(CONFIG);
const auth = auth_1.AuthJS.auth(CONFIG, UTILITY);
const servertime = servertime_1.ServerTimeJS.servertime(CONFIG, UTILITY);
const MASTERDATA = masterdata_1.MasterDataJS.masterdata(CONFIG, UTILITY, tags_json_1.default);
socketlistener_1.SocketListenerJS.socketlistener(CONFIG).init();
const emitter = socketlistener_1.SocketListenerJS.emitter;
const log = bunyan_1.default.createLogger({
    name: `index`,
    level: CONFIG.logger.loglevel,
});
const stationMap = [];
const substationTags = tags_json_1.default.substationtags;
const handshake = handshake_1.default.handshake();
UTILITY.emitter.on('sessionExpired', () => {
    log.info(`Session Expired trying to reAuthenticate`);
    auth.getAuthentication();
});
emitter.on('substation', (data) => tslib_1.__awaiter(void 0, void 0, void 0, function* () {
    for (var i = 0; i < stationMap.length; i++) {
        if (data[substationTags.NAME_TAG] == stationMap[i].substationname) {
            log.error(`Substation update event triggered`);
            stationMap[i].updateStation(data);
        }
    }
}));
function initStation() {
    for (var i = 0; i < MASTERDATA.stationInfo.length; i++) {
        const obj = station_1.default.stations(CONFIG, MASTERDATA.stationInfo[i], UTILITY, tags_json_1.default, emitter, kafka_service_1.default);
        setTimeout(() => {
            obj.initEmitter();
            handshake.inithandshake();
        }, 5 * 1000);
        stationMap.push(obj);
    }
}
function getMasterData() {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        yield MASTERDATA.getStationInformation();
        initStation();
    });
}
auth_1.AuthJS.emitter.on('init', () => {
    log.error(`Authentication done successfully`);
    servertime.getServerTime();
    getMasterData();
    
    
});
auth.getAuthentication();
