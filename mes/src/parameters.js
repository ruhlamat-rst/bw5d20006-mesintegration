"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const bunyan_1 = tslib_1.__importDefault(require("bunyan"));
const tags_json_1 = tslib_1.__importDefault(require("../tags/tags.json"));
const config_json_1 = tslib_1.__importDefault(require("../config/config.json"));
const UTILITY = tslib_1.__importStar(require("../utils/utility"));
const element_service_1 = tslib_1.__importDefault(require("../service/element.service"));
function parameters(config = config_json_1.default, substation, utility = UTILITY, tags = tags_json_1.default, emitter) {
    const log = bunyan_1.default.createLogger({
        name: 'Parameters',
        level: config.logger.loglevel,
    });
    const elements = config.elements;
    const defaults = config.defaults;
    const parameterTags = tags.parametertags;
    const substationTags = tags.substationtags;
    let retryServerTimer = defaults.retryServerTimer || 10;
    let lastSocketEventTime = new Date().valueOf();
    return {
        parametersList: [],
        initEmitter() {
            emitter.on('parameters', (data) => tslib_1.__awaiter(this, void 0, void 0, function* () {
                const diff = new Date().valueOf() - lastSocketEventTime;
                if (diff > 5 * 1000) {
                    log.error(`Socket Event received for parameters`);
                    lastSocketEventTime = new Date().valueOf();
                    yield utility.setTimer(5);
                    this.getParameters();
                }
            }));
        },
        getParameters() {
            return tslib_1.__awaiter(this, void 0, void 0, function* () {
                const elementName = elements.parameters || 'parameters';
                try {
                    log.error(`Get Parameters`);
                    const lineid = substation[substationTags.LINEID_TAG];
                    const sublineid = substation[substationTags.SUBLINEID_TAG];
                    const substationid = substation[substationTags.SUBSTATIONID_TAG];
                    let query = `query=${[parameterTags.LINEID_TAG]}==${lineid}%26%26${[
                        parameterTags.SUBLINEID_TAG,
                    ]}=="${sublineid}"%26%26${[
                        parameterTags.SUBSTATIONID_TAG,
                    ]}=="${substationid}"`;
                    const response = yield element_service_1.default.getElementRecords(elementName, query);
                    if (response.data && response.data.results) {
                        this.parametersList = response.data.results;
                    }
                    else {
                        log.error(`parameter not found for elementName : ${elementName} ${JSON.stringify(response.data)}`);
                        utility.checkSessionExpired(response.data);
                        yield utility.setTimer(retryServerTimer);
                        yield this.getParameters();
                    }
                }
                catch (ex) {
                    log.error(`Exception to fetch parameter for element : ${elementName}`);
                    const messageObject = ex.response ? ex.response.data : ex;
                    log.error(messageObject);
                    yield utility.setTimer(retryServerTimer);
                    yield this.getParameters();
                }
            });
        },
    };
}
exports.default = {
    parameters,
};
