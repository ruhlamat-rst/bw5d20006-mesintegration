"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const lodash_1 = tslib_1.__importDefault(require("lodash"));
const bunyan_1 = tslib_1.__importDefault(require("bunyan"));
const tags_json_1 = tslib_1.__importDefault(require("../tags/tags.json"));
const config_json_1 = tslib_1.__importDefault(require("../config/config.json"));
const UTILITY = tslib_1.__importStar(require("../utils/utility"));
const interface_service_1 = tslib_1.__importDefault(require("../service/interface.service"));
const moment_1 = tslib_1.__importDefault(require("moment"));
const element_service_1 = tslib_1.__importDefault(require("../service/element.service"));
const kafka_service_1 = tslib_1.__importDefault(require("../service/kafka.service"));
const parameters_1 = tslib_1.__importDefault(require("./parameters"));
function stations(config = config_json_1.default, substation, utility = UTILITY, tags = tags_json_1.default, emitter, kafka) {
    const parameterTags = tags.parametertags;
    const stationTags = tags.substationtags;
    let lineid = substation[stationTags.LINEID_TAG];
    let sublineid = substation[stationTags.SUBLINEID_TAG];
    let substationname = substation[stationTags.NAME_TAG];
    let substationid = substation[stationTags.SUBSTATIONID_TAG];
    let ismainline = substation[stationTags.ISMAINLINE_TAG];
    const log = bunyan_1.default.createLogger({
        name: substationid,
        level: config.logger.loglevel,
    });
    const topic_name = `interface-${substationid}`;
    const PARAMETERS = parameters_1.default.parameters(config, substation, utility, tags, emitter);
    PARAMETERS.initEmitter();
    PARAMETERS.getParameters();
    return {
        substationid: substationid,
        substationname: substationname,
        updateStation(stationinfo) {
            substation = stationinfo;
            lineid = substation[stationTags.LINEID_TAG];
            sublineid = substation[stationTags.SUBLINEID_TAG];
            substationname = substation[stationTags.NAME_TAG];
            substationid = substation[stationTags.SUBSTATIONID_TAG];
            ismainline = substation[stationTags.ISMAINLINE_TAG];
            this.substationid = substationid;
            this.substationname = substationname;
        },
        initEmitter() {
            kafka_service_1.default.initClient(topic_name);
            emitter.on('checkout', (data) => {
                if (data.substationid === substationid) {
                    log.error(`checkout event triggered ${JSON.stringify(data)}`);
                    this.checkoutTrigger(data);
                }
            });
            emitter.on('equipmentalarm', (data) => {
                log.error(`equipmentalarm event triggered ${JSON.stringify(data)}`);
                if (data.substationid === substationid) {
                    this.toCommitEquipmentAlarm(data);
                }
            });
            emitter.on('stationstatus', (data) => {
               
                if (data.substationid === substationid) {
                    log.error(`stationstatus event triggered ${JSON.stringify(data)}`);
                    this.toCommitStationStatus(data);
                }
            });
            emitter.on('cyclerunning', (data) => {
                
                if (data.substationid === substationid) {
                    log.error(`cyclerunning event triggered ${JSON.stringify(data)}`);
                    if (data.state == 'In Progress') {
                        this.toCommitOrderInProduction(data);
                    }
                    else {
                        this.toCommitOrderCompleted(data);
                    }
                }
            });
            emitter.on('personelcheck', (data) => {
                
                if (data.substationid === substationid) {
                    log.error(`personnelqualification event triggered ${JSON.stringify(data)}`);
                    this.toCommitPersonnelQualification(data);
                }
            });
        },
        checkoutTrigger(data) {
            this.toCommitMaterialConsumption(data);
            if (substationid === 'substation-211') {
                this.toCommitOrderInformation(data);
            }
            this.toCommitProductionInspectionData(data);
        },
        toCommitOrderInProduction(data) {
            return tslib_1.__awaiter(this, void 0, void 0, function* () {
                let payload;
                try {
                    let sublinename = '';
                    let sublinequery = `query=id=="${data.sublineid}"`;
                    const sublinerecord = yield element_service_1.default.getElementRecords(config.elements.subline || 'subline', sublinequery);
                    if (sublinerecord.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                        if (sublinerecord.data &&
                            sublinerecord.data.results &&
                            sublinerecord.data.results.length > 0) {
                            sublinename = sublinerecord.data.results[0]['name'];
                        }
                        payload = {
                            ProductionLineNo: `${config.stationprefix}${ismainline ? '' : '-'}${ismainline ? '' : sublinename}`,
                            WipOrderNo: data.ordername,
                            Equipment: `BW754_${config.stationprefix}${ismainline ? '' : '-'}${ismainline ? '' : sublinename}_${substationname}`,
                            TempParentSerialNo: data.mainid,
                            SystemTime: moment_1.default(data.starttime).format('YYYY-MM-DD HH:mm:ss.SSS'),
                        };
                        log.error(`Cyclerunning Start payload ${JSON.stringify(payload)}`);
                        const response = yield interface_service_1.default.commitOrderInProduction(payload);
                        log.error(`Cyclerunning Start response ${response.status} : ${response.status} : ${JSON.stringify(response.data)}`);
                        if (response.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                            if (response.data && response.data.Outputs) {
                                log.error(`Order In Production Send Successfully for ${substationid}`);
                            }
                            else {
                                log.error(`Error for Order In Production Sending  for ${substationid}`);
                            }
                        }
                        else {
                            log.error(`Error for Order In Production Sending for ${substationname}`);
                            kafka.sendMessage(topic_name, {
                                method: 'commitOrderInProduction',
                                payload: payload,
                            });
                        }
                    }
                    else {
                        utility.checkSessionExpired(sublinerecord.data);
                        this.toCommitOrderInProduction(data);
                    }
                }
                catch (error) {
                    log.error(`Error for Order In Production Sending for : ${data.mainid}`);
                    if(error. isAxiosError){
                       kafka.sendMessage(topic_name, {
                                method: 'commitOrderInProduction',
                                payload: payload,
                       });
                    }
                }
            });
        },
        toCommitMaterialConsumption(data) {
            return tslib_1.__awaiter(this, void 0, void 0, function* () {
                let payload;
                const batchparameters = this.getParametersByCategory(config.defaults.batchID);
                try {
                    let parameters = [];
                    let query = `query=${tags_json_1.default.componenttags.MAINID_TAG}=="${data.mainid}"%26%26${tags_json_1.default.componenttags.SUBSTATIONID_TAG}=="${substationid}"`;
                    query += `%26%26${tags_json_1.default.componenttags.PARAMETERCATEGORY_TAG}=="${config.defaults.batchID}"`;
                    const response = yield element_service_1.default.getElementRecords(config.elements.component, query);
                    if (response.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                        if (response.data &&
                            response.data.results) {
                            log.error(`Parameters fetched successfully`);
                            parameters = response.data.results;
                            // const batchData = this.mapData(parameters, batchparameters);
                            const mappingdata = this.mapBatchid(parameters,batchparameters);
                            if (substationid == 'substation-195' ||
                                substationid == 'substation-207') {
                                let componentname = '';
                                let asmstation = '';
                                if (substationid == 'substation-207') {
                                    componentname = 'asm_main_hub_welded_material_id';
                                    asmstation = 'substation-167';
                                }
                                else {
                                    componentname = 'asm_rotor_carrier_material_id';
                                    asmstation = 'substation-215';
                                }
                                let asmquery = `query=${tags_json_1.default.componenttags.MAINID_TAG}=="${data.mainid}"%26%26${tags_json_1.default.componenttags.SUBSTATIONID_TAG}=="${substationid}"`;
                                asmquery += `%26%26${tags_json_1.default.componenttags.PARAMETERCATEGORY_TAG}=="${config.defaults.subassemblyID}"`;
                                const asmresponse = yield element_service_1.default.getElementRecords(config.elements.component, asmquery);
                                let subassemblyid = '';
                                let lotcode = '';
                                if (asmresponse.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                                    if (asmresponse.data &&
                                        asmresponse.data.results &&
                                        asmresponse.data.results.length > 0) {
                                        subassemblyid = asmresponse.data.results[0]['componentvalue'];
                                        if (subassemblyid) {
                               let lotquery = `query=${tags_json_1.default.componenttags.MAINID_TAG}=="${subassemblyid}"%26%26${tags_json_1.default.componenttags.SUBSTATIONID_TAG}=="${asmstation}"`;
                                            lotquery += `%26%26${tags_json_1.default.componenttags.PARAMETERCATEGORY_TAG}=="${config.defaults.batchID}"`;
                                            lotquery += `%26%26${tags_json_1.default.componenttags.COMPONENTNAME_TAG}=="${componentname}"`;
                                            const lotresponse = yield element_service_1.default.getElementRecords(config.elements.component, lotquery);
                                            debugger;
if (lotresponse.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                                                if (lotresponse.data &&
                                                    lotresponse.data.results &&
                                                    lotresponse.data.results.length > 0) {
                                                    lotcode = lotresponse.data.results[0]['componentvalue'];
                                                    mappingdata.ComponentCode.push(lotcode);
                                                    mappingdata.LotNo.push(subassemblyid);
                                                }
                                            }
                                            else {
                                                utility.checkSessionExpired(lotresponse.data);
                                                this.toCommitMaterialConsumption(data);
                                                return;
                                            }
                                        }
                                    }
                                }
                                else {
                                    utility.checkSessionExpired(asmresponse.data);
                                    this.toCommitMaterialConsumption(data);
                                    return;
                                }
                            }
                            let sublinename = '';
                            let sublinequery = `query=id=="${data.sublineid}"`;
                            const sublinerecord = yield element_service_1.default.getElementRecords(config.elements.subline || 'subline', sublinequery);
                            if (sublinerecord.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                                if (sublinerecord.data &&
                                    sublinerecord.data.results &&
                                    sublinerecord.data.results.length > 0) {
                                    sublinename = sublinerecord.data.results[0]['name'];
                                }
                                payload = {
                                    ProductionLineNo: `${config.stationprefix}${ismainline ? '' : '-'}${ismainline ? '' : sublinename}`,
                                    WipOrderNo: data.ordername,
                                    Equipment: `BW754_${config.stationprefix}${ismainline ? '' : '-'}${ismainline ? '' : sublinename}_${substationname}`,
                                    TempSerialNo: data.mainid,
                                    SystemTime: moment_1.default().format('YYYY-MM-DD HH:mm:ss.SSS'),
                                    ComponentCode: mappingdata.ComponentCode,
                                    LotNo: mappingdata.LotNo,
                                };
log.error(`MaterialConsumption payload ${JSON.stringify(payload)}`);
                                const response1 = yield interface_service_1.default.commitMaterialConsumption(payload);
log.error(`MaterialConsumption response ${response1.status} : ${JSON.stringify(response1.data)}}`);
                                if (response1.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                                    if (response1.data &&
                                        response1.data.Outputs) {
                                        log.error(`Order In Production Send Successfully for ${substationid}`);
                                    }
                                    else {
                                        log.error(`Error for Order In Production Sending  for ${substationid}`);
                                    }
                                }
                                else {
                                    log.error(`Error for Order In Production Sending for ${substationname}`);
                                    kafka.sendMessage(topic_name, {
                                        method: 'commitMaterialConsumption',
                                        payload: payload,
                                    });
                                }
                            }
                            else {
                                utility.checkSessionExpired(sublinerecord.data);
                                this.toCommitMaterialConsumption(data);
                            }
                        }
                        else {
                            log.error(`Parameters not found  ${JSON.stringify(response.data)}`);
                            parameters = [];
                        }
                    }
                    else {
                        log.error(`Error in getting data of Parameters for mainid : ${data.mainid} ${JSON.stringify(response.data)}`);
                        utility.checkSessionExpired(response.data);
                        parameters = [];
                        this.toCommitMaterialConsumption(data);
                    }
                }
                catch (error) {
                    log.error(`Exception to fetch ComponentId for mainid : ${data.mainid}`);
                    const messageObject = error.response ? error.response.data : error;
                    log.error(messageObject);
                    if (error.code == 'EHOSTUNREACH') {
                      kafka.sendMessage(topic_name, {
                          method: 'commitMaterialConsumption',
                          payload: payload,
                      });
                  }
                }
            });
        },
        toCommitOrderCompleted(data) {
            return tslib_1.__awaiter(this, void 0, void 0, function* () {
                let payload;
                try {
                    let sublinename = '';
                    let sublinequery = `query=id=="${data.sublineid}"`;
                    const sublinerecord = yield element_service_1.default.getElementRecords(config.elements.subline || 'subline', sublinequery);
                    if (sublinerecord.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                        if (sublinerecord.data &&
                            sublinerecord.data.results &&
                            sublinerecord.data.results.length > 0) {
                            sublinename = sublinerecord.data.results[0]['name'];
                        }
                        payload = {
                            ProductionLineNo: `${config.stationprefix}${ismainline ? '' : '-'}${ismainline ? '' : sublinename}`,
                            WipOrderNo: data.ordername,
                            Equipment: `BW754_${config.stationprefix}${ismainline ? '' : '-'}${ismainline ? '' : sublinename}_${data.substationname}`,
                            TempParentSerialNo: data.mainid,
                            SystemTime: moment_1.default(data.endtime).format('YYYY-MM-DD HH:mm:ss.SSS'),
                            Result: data.checkoutresult,
                            FinalStep: config.defaults.finalstep || '1',
                        };
                        log.error(`Cyclerunning End payload ${JSON.stringify(payload)}`);
                        const response = yield interface_service_1.default.commitOrderCompleted(payload);
                        log.error(`Cyclerunning End response ${response.status} : ${JSON.stringify(response.data)}`);
                        if (response.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                            if (response.data && response.data.Outputs) {
                                log.error(`Order In Production Send Successfully for ${substationid}`);
                            }
                            else {
                                log.error(`Error for Order In Production Sending  for ${substationid}`);
                            }
                        }
                        else {
                            log.error(`Error for Order In Production Sending for ${substationname}`);
                            kafka.sendMessage(topic_name, {
                                method: 'commitOrderCompleted',
                                payload: payload,
                            });
                        }
                    }
                    else {
                        utility.checkSessionExpired(sublinerecord.data);
                        this.toCommitOrderCompleted(data);
                    }
                }
                catch (error) {
                    log.error(`Error for Order In Production Sending for ${substationname}`);
                    if (error.code == 'EHOSTUNREACH') {
                      kafka.sendMessage(topic_name, {
                          method: 'commitOrderCompleted',
                          payload: payload,
                      });
                  }
                }
            });
        },
        toCommitOrderInformation(data) {
            return tslib_1.__awaiter(this, void 0, void 0, function* () {
                let payload;
                try {
                    let sublinename = '';
                    let sublinequery = `query=id=="${data.sublineid}"`;
                    const sublinerecord = yield element_service_1.default.getElementRecords(config.elements.subline || 'subline', sublinequery);
                    if (sublinerecord.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                        if (sublinerecord.data &&
                            sublinerecord.data.results &&
                            sublinerecord.data.results.length > 0) {
                            sublinename = sublinerecord.data.results[0]['name'];
                        }
                        payload = {
                            ProductionLineNo: `${config.stationprefix}${ismainline ? '' : '-'}${ismainline ? '' : sublinename}`,
                            WipOrderNo: data.ordername,
                            Equipment: `BW754_${config.stationprefix}${ismainline ? '' : '-'}${ismainline ? '' : sublinename}_${substationname}`,
                            TempSerialNo: data.mainid,
                            SystemTime: moment_1.default().format('YYYY-MM-DD HH:mm:ss.SSS'),
                            SerialNo: data.completedproductid || '',
                        };
                        log.error(`Final Station Report Payload ${JSON.stringify(payload)}`);
                        const response = yield interface_service_1.default.commitOrderInformation(payload);
                        log.error(`Final Station Report response ${response.status} : ${JSON.stringify(response.data)}`);
                        if (response.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                            if (response.data && response.data.Outputs) {
                                log.error(`Order In Production Send Successfully for ${substationid}`);
                            }
                            else {
                                log.error(`Error for Order In Production Sending  for ${substationid}`);
                            }
                        }
                        else {
                            log.error(`Error for Order In Production Sending for ${substationname}`);
                            kafka.sendMessage(topic_name, {
                                method: 'commitOrderInformation',
                                payload: payload,
                            });
                        }
                    }
                    else {
                        utility.checkSessionExpired(sublinerecord.data);
                        this.toCommitOrderInformation(data);
                    }
                }
                catch (error) {
                    log.error(`Error for Order In Production Sending for ${substationname}`);
                    if (error.code == 'EHOSTUNREACH') {
                      kafka.sendMessage(topic_name, {
                          method: 'commitOrderInformation',
                          payload: payload,
                      });
                  }
                }
            });
        },
        toCommitProductionInspectionData(data) {
            return tslib_1.__awaiter(this, void 0, void 0, function* () {
                let payload;
                const processparameters = this.getParametersByCategory(config.defaults.subprocessparameters);
                const elementName = substationid;
                try {
                    let parameters = [];
                    let query = `query=${tags_json_1.default.checkouttags.MAINID_TAG}=="${data.mainid}"%26%26${tags_json_1.default.checkouttags.SUBSTATIONID_TAG}=="${substationid}"`;
                    const response = yield element_service_1.default.getElementRecords(elementName, query);
                    if (response.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                        if (response.data &&
                            response.data.results &&
                            response.data.results.length > 0) {
                            log.error(`Parameters fetched successfully`);
                            parameters = response.data.results[0];
                            const processData = this.mapData(parameters, processparameters);
                            const mappingdata = this.mapPayload(processData, config.defaults.mappingway);
                            let sublinename = '';
                            let sublinequery = `query=id=="${data.sublineid}"`;
                            const sublinerecord = yield element_service_1.default.getElementRecords(config.elements.subline || 'subline', sublinequery);
                            if (sublinerecord.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                                if (sublinerecord.data &&
                                    sublinerecord.data.results &&
                                    sublinerecord.data.results.length > 0) {
                                    sublinename = sublinerecord.data.results[0]['name'];
                                }
                                payload = {
                                    ProductionLineNo: `${config.stationprefix}${ismainline ? '' : '-'}${ismainline ? '' : sublinename}`,
                                    WipOrderNo: data.ordername,
                                    Equipment: `BW754_${config.stationprefix}${ismainline ? '' : '-'}${ismainline ? '' : sublinename}_${substationname}`,
                                    TempSerialNo: data.mainid,
                                    SystemTime: moment_1.default().format('YYYY-MM-DD HH:mm:ss.SSS'),
                                    CharacteristicCode: mappingdata.CharacteristicCode,
                                    CharacteristicVarValue: mappingdata.CharacteristicVarValue,
                                    CharacteristicAttValue: mappingdata.CharacteristicAttValue,
                                    DefectCode: mappingdata.DefectCode,
                                    CharacteristicResult: mappingdata.CharacteristicResult,
                                    WipOperStep: mappingdata.WipOperStep,
                                };
                               log.error(`ProductionInspectionData Payload ${JSON.stringify(payload)}`);
                                const response1 = yield interface_service_1.default.commitProductionInspectionData(payload);
                                log.error(`ProductionInspectionData response ${response1.status} : ${JSON.stringify(response1.data)}}`);
                                if (response1.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                                    if (response1.data &&
                                        response1.data.Outputs) {
                                        log.error(`Parameters Send Successfully for ${substationid}`);
                                    }
                                    else {
                                        log.error(`Error for Order In Parameters Sending  for ${substationid}`);
                                    }
                                }
                                else {
                                    log.error(`Error for Order In Parameters Sending for ${substationname}`);
                                        kafka.sendMessage(topic_name, {
                                            method: 'commitProductionInspectionData',
                                            payload: payload,
                                        });
                                }
                            }
                            else {
                                utility.checkSessionExpired(sublinerecord.data);
                                this.toCommitProductionInspectionData(data);
                            }
                        }
                        else {
                            log.error(`Parameters not found  ${JSON.stringify(response.data)}`);
                            parameters = [];
                        }
                    }
                    else {
                        log.error(`Error in getting data of Parameters for mainid : ${data.mainid} ${JSON.stringify(response.data)}`);
                        utility.checkSessionExpired(response.data);
                        parameters = [];
                        this.toCommitProductionInspectionData(data);
                    }
                }
                catch (error) {
                    log.error(`Exception to fetch Parameters for mainid : ${data.mainid}`);
                    const messageObject = error.response ? error.response.data : error;
                    log.error(messageObject);
                    if (error.code == 'EHOSTUNREACH') {
                      kafka.sendMessage(topic_name, {
                          method: 'commitProductionInspectionData',
                          payload: payload,
                      });
                  }
                }
            });
        },
        toCommitEquipmentAlarm(data) {
            return tslib_1.__awaiter(this, void 0, void 0, function* () {
                let payload; 
                try {
                    payload = {
                        ProductionLineNo: `${config.stationprefix}${ismainline ? '' : '-'}${ismainline ? '' : data.sublinename}`,
                        Equipment: `BW754_${config.stationprefix}${ismainline ? '' : '-'}${ismainline ? '' : data.sublinename}_${data.substationname}`,
                        ReasonCode: data.reasoncode,
                        AlarmType: data.alarmType || 1,
                        AlarmLevel: data.alarmLevel || 1,
                        SystemTime: moment_1.default(data.starttime).format('YYYY-MM-DD HH:mm:ss.SSS'),
                    };
                    log.error(`EquipmentAlarm Payload ${JSON.stringify(payload)}`);
                    const response = yield interface_service_1.default.commitEquipmentAlarm(payload);
                    log.error(`EquipmentAlarm response ${response.status} : ${JSON.stringify(response.data)}`);
                    if (response.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                        if (response.data && response.data.Outputs) {
                            log.error(`Equipment Alarm Send Successfully for ${substationid}`);
                        }
                        else {
                            log.error(`Error for Equipment Alarm Sending  for ${substationid}`);
                        }
                    }
                    else {
                        log.error(`Error for Equipment Alarm Sending for ${substationname}`);
                        kafka.sendMessage(topic_name, {
                            method: 'commitEquipmentAlarm',
                            payload: payload,
                        });
                    }
                }
                catch (error) {
                    log.error(`Error for Equipment Alarm Sending for : ${data.mainid}`);
                    const messageObject = error.response ? error.response.data : error;
                    log.error(messageObject);
                    if (error.code == 'EHOSTUNREACH') {
                      kafka.sendMessage(topic_name, {
                          method: 'commitEquipmentAlarm',
                          payload: payload,
                      });
                  }
                }
            });
        },
        toCommitStationStatus(data) {
            return tslib_1.__awaiter(this, void 0, void 0, function* () {
                let payload;
                try {
                    payload = {
                        ProductionLineNo: `${config.stationprefix}${ismainline ? '' : '-'}${ismainline ? '' : data.sublinename}`,
                        ReasonCode: data.status,
                        Equipment: `BW754_${config.stationprefix}${ismainline ? '' : '-'}${ismainline ? '' : data.sublinename}_${data.substationname}`,
                        SystemTime: moment_1.default(data.starttime).format('YYYY-MM-DD HH:mm:ss.SSS'),
                    };
                    log.error(`EquipmentStatus Payload ${JSON.stringify(payload)}`);
                    const response = yield interface_service_1.default.changeEquipmentStatus(payload);
                    log.error(`EquipmentStatus response ${response.status} : ${JSON.stringify(response.data)}`);
                    if (response.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                        if (response.data && response.data.Outputs) {
                            log.error(`StationStatus Send Successfully for ${substationname}`);
                        }
                        else {
                            log.error(`Error for StationStatus Sending  for ${substationname}`);
                        }
                    }
                    else {
                        log.error(`Error for StationStatus Sending for ${substationname}`);
                        kafka.sendMessage(topic_name, {
                            method: 'changeEquipmentStatus',
                            payload: payload,
                        });
                    }
                }
                catch (error) {
                    log.error(`Error for StationStatus Sending for : ${data.mainid}`);
                    const messageObject = error.response ? error.response.data : error;
                    log.error(messageObject);
                    if (error.code == 'EHOSTUNREACH') {
                      kafka.sendMessage(topic_name, {
                          method: 'changeEquipmentStatus',
                          payload: payload,
                      });
                  }
                }
            });
        },
        toCommitPersonnelQualification(data) {
            return tslib_1.__awaiter(this, void 0, void 0, function* () {
               let payload;
                try { 
                    payload = {
                        EmployeeNo: data.personelcheckresult,
                        WipOrderNo: data.ordername,
                        Equipment: `BW754_${config.stationprefix}${ismainline ? '' : '-'}${ismainline ? '' : data.sublinename}_${data.substationname}`,
                        SystemTime: moment_1.default(data.starttime || new Date()).format('YYYY-MM-DD HH:mm:ss.SSS'),
                    };
                   log.error(`PersonnelQualification Payload ${JSON.stringify(payload)}`);
                    const response = yield interface_service_1.default.checkPersonnelQualification(payload);
                    log.error(`PersonnelQualification response ${response.status} : ${JSON.stringify(response.data)}`);
                    if (response.status === utility.HTTP_STATUS_CODE.SUCCESS) {
                        if (response.data && response.data.Outputs) {
                            log.error(`PersonnelQualification Send Successfully for ${substationid}`);
                        }
                        else {
                            log.error(`Error for PersonnelQualification Sending  for ${substationid}`);
                        }
                    }
                    else {
                        log.error(`Error for PersonnelQualification Sending for ${substationname}`);
                        kafka.sendMessage(topic_name, {
                            method: 'checkPersonnelQualification',
                            payload: payload,
                        });
                    }
                }
                catch (error) {
                    log.error(`Error for PersonnelQualification Sending for : ${data.mainid}`);
                    const messageObject = error.response ? error.response.data : error;
                    log.error(messageObject);
                    if (error.code == 'EHOSTUNREACH') {
                      kafka.sendMessage(topic_name, {
                          method: 'checkPersonnelQualification',
                          payload: payload,
                      });
                  }
                }
            });
        },
        getParametersByCategory(category) {
            const parameters = [];
            PARAMETERS.parametersList.filter((item) => {
                if (item[parameterTags.PARAMETERCATEGORY_TAG] === category) {
                    parameters.push(item);
                }
            });
            return parameters;
        },
        mapData(data, parameters) {
            const mapData = {};
            parameters.forEach((element) => {
                const key = element.name;
                if (lodash_1.default.has(data, key)) {
                    mapData[key] = data[key];
                }
            });
            return mapData;
        },
        getParameterName(data, str) {
            const parameters = [];
            for (const key in data) {
                if (Object.prototype.hasOwnProperty.call(data, key)) {
                    const keyarr = key.split('_');
                    if (keyarr.length >= 2) {
                        if (keyarr[keyarr.length - 1] == str) {
                            keyarr.pop();
                            parameters.push(keyarr.join('_'));
                        }
                    }
                }
            }
            return parameters;
        },
        mapPayload(data, type) {
            let mappingdata = {
                CharacteristicCode: [],
                CharacteristicVarValue: [],
                CharacteristicAttValue: [],
                DefectCode: [],
                CharacteristicResult: [],
                WipOperStep: [],
            };
            let parameters = this.getParameterName(data, 'result');
            if (type == 1) {
                parameters.forEach((item) => {
                    mappingdata.CharacteristicCode.push(item.trim());
                    mappingdata.DefectCode.push(data[`${item}_ng_code`] || '');
                    mappingdata.CharacteristicResult.push(data[`${item}_result`] || 0);
                    mappingdata.WipOperStep.push('1');
                    if (data[`${item}_value`]) {
                        if (typeof data[`${item}_value`] === 'number' &&
                            !Number.isNaN(data[`${item}_value`])) {
                            mappingdata.CharacteristicVarValue.push(data[`${item}_value`].toString());
                            mappingdata.CharacteristicAttValue.push('');
                        }
                        else {
                            mappingdata.CharacteristicVarValue.push('');
                            mappingdata.CharacteristicAttValue.push(data[`${item}_value`].toString());
                        }
                    }
                    else {
                        mappingdata.CharacteristicVarValue.push('');
                        mappingdata.CharacteristicAttValue.push('');
                    }
                });
            }
            else if (type == 2) {
                parameters.forEach((item) => {
                    mappingdata.CharacteristicCode.push(`${item.trim()}_torque`);
                    mappingdata.CharacteristicVarValue.push(data[`${item}_torque`].toString());
                    mappingdata.CharacteristicAttValue.push('');
                    mappingdata.DefectCode.push(data[`${item}_ng_code`] || '');
                    mappingdata.CharacteristicResult.push(data[`${item}_result`]);
                    mappingdata.WipOperStep.push('1');
                    mappingdata.CharacteristicCode.push(`${item.trim()}_angle`);
                    mappingdata.CharacteristicVarValue.push(data[`${item}_angle`].toString());
                    mappingdata.CharacteristicAttValue.push('');
                    mappingdata.DefectCode.push(data[`${item}_ng_code`] || '');
                    mappingdata.CharacteristicResult.push(data[`${item}_result`]);
                    mappingdata.WipOperStep.push('1');
                    mappingdata.CharacteristicCode.push(`${item.trim()}_counter`);
                    mappingdata.CharacteristicVarValue.push(data[`${item}_counter`].toString());
                    mappingdata.CharacteristicAttValue.push('');
                    mappingdata.DefectCode.push(data[`${item}_ng_code`] || '');
                    mappingdata.CharacteristicResult.push(data[`${item}_result`]);
                    mappingdata.WipOperStep.push('1');
                });
            }
            return mappingdata;
        },
        mapBatchid(data, parameters) {
            let mappingdata = {
                ComponentCode: [],
                LotNo: [],
            };
            let parameternames = parameters
                .filter((item) => {
                return item.name.includes('material_id');
            })
                .map((item) => {
                return item.name.replace('_material_id','');
            });
            parameternames.forEach((item) => {
                let componentcode = `${item}_material_id`;
                let lotcode = `${item}_lot_code`;
                let componentcodevalue = data.find((item) => {
                    return item[tags.componenttags.COMPONENTNAME_TAG] == componentcode;
                });
                let lotcodevalue = data.find((item) => {
                    return item[tags.componenttags.COMPONENTNAME_TAG] == lotcode;
                });
                if (componentcodevalue) {
                    mappingdata.ComponentCode.push(componentcodevalue[tags.componenttags.COMPONENTVALUE_TAG]);
                }
                else {
                    mappingdata.ComponentCode.push('');
                }
                if (lotcodevalue) {
                    mappingdata.LotNo.push(lotcodevalue[tags.componenttags.COMPONENTVALUE_TAG]);
                }
                else {
                    mappingdata.LotNo.push('');
                }
            });

            return mappingdata;
        },
    };
}
exports.default = { stations };
