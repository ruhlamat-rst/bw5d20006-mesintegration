"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const bunyan_1 = tslib_1.__importDefault(require("bunyan"));
const querystring_1 = tslib_1.__importDefault(require("querystring"));
const config_json_1 = tslib_1.__importDefault(require("../config/config.json"));
const UTILITY = tslib_1.__importStar(require("../utils/utility"));
const interface_service_1 = tslib_1.__importDefault(require("../service/interface.service"));
const moment_1 = tslib_1.__importDefault(require("moment"));
const element_service_1 = tslib_1.__importDefault(require("../service/element.service"));
const kafka_service_1 = tslib_1.__importDefault(require("../service/kafka.service"));
function handshake() {
    const log = bunyan_1.default.createLogger({
        name: 'mes-handshake',
        level: config_json_1.default.logger.loglevel,
    });
    return {
        communication: -1,
        online: -1,
        inithandshake() {
            var _a, _b, _c;
            return tslib_1.__awaiter(this, void 0, void 0, function* () {
                try {
                    const response = yield interface_service_1.default.handshake();
                    if (response.status === UTILITY.HTTP_STATUS_CODE.SUCCESS) {
                        if (this.communication == 0 || this.communication == -1) {
                            this.communication = 1;
                            if (this.online == 0 || this.online == -1) {
                                (_a = kafka_service_1.default.consumer) === null || _a === void 0 ? void 0 : _a.resume();
                                this.online = 1;
                            }
                            log.error(`consumer start`);
                        }
                    }
                    else if (response.status === UTILITY.HTTP_STATUS_CODE.INTERNAL_SERVER_ERROR) {
                        if (this.online == 1 || this.online == -1) {
                            (_b = kafka_service_1.default.consumer) === null || _b === void 0 ? void 0 : _b.pause();
                            log.error(`consumer closed`);
                            this.online = 0;
                        }
                        if (this.communication == 1 || this.communication == -1) {
                            this.communication = 0;
                        }
                        yield UTILITY.setTimer(10);
                        this.inithandshake();
                    }
                    else {
                        if (this.communication == 1 || this.communication == -1) {
                            this.communication = 0;
                        }
                    }
                    yield UTILITY.setTimer(10);
                    this.inithandshake();
                }
                catch (error) {
                    if (this.online == 1 || this.online == -1) {
                        (_c = kafka_service_1.default.consumer) === null || _c === void 0 ? void 0 : _c.pause();
                        log.error(`consumer closed`);
                        this.online = 0;
                    }
                    if (this.communication == 1 || this.communication == -1) {
                        this.communication = 0;
                    }
                    yield UTILITY.setTimer(10);
                    this.inithandshake();
                }
            });
        },
        writeRecordInSWX() {
            return tslib_1.__awaiter(this, void 0, void 0, function* () {
                let payload = {
                    state: 'In Progress',
                    starttime: moment_1.default().valueOf(),
                    assetid: config_json_1.default.assetid,
                };
                log.trace(`mesoffline Payload ${JSON.stringify(payload)}`);
                const elementName = config_json_1.default.elements.mesoffline || 'mesoffline';
                try {
                    let query = `query=state=="${querystring_1.default.escape('In Progress')}"`;
                    const inProgressed = yield element_service_1.default.getElementRecords(elementName, query);
                    if (inProgressed.status === UTILITY.HTTP_STATUS_CODE.SUCCESS) {
                        if (inProgressed.data &&
                            inProgressed.data.results &&
                            inProgressed.data.results.length <= 0) {
                            const response = yield element_service_1.default.createElementRecord(elementName, payload);
                            if (response.status === UTILITY.HTTP_STATUS_CODE.SUCCESS) {
                                log.error(`Record saved successfully in ShopWorx for cyclerunning ${JSON.stringify(response.data)}`);
                            }
                            else {
                                log.error(`Error in writing cyclerunning data in ShopWorx ${JSON.stringify(response.data)}`);
                                UTILITY.checkSessionExpired(response.data);
                                yield UTILITY.setTimer(config_json_1.default.defaults.retryServerTimer);
                                yield this.writeRecordInSWX();
                            }
                        }
                    }
                    else {
                        log.error(`Error in writing cyclerunning data in ShopWorx ${JSON.stringify(inProgressed.data)}`);
                        UTILITY.checkSessionExpired(inProgressed.data);
                        yield UTILITY.setTimer(config_json_1.default.defaults.retryServerTimer);
                        yield this.writeRecordInSWX();
                    }
                }
                catch (error) {
                    log.error(`Exception in writing cyclerunning data in ShopWorx ${error}`);
                    yield UTILITY.setTimer(config_json_1.default.defaults.retryServerTimer);
                    yield this.writeRecordInSWX();
                }
            });
        },
        updateRecordInSWX() {
            return tslib_1.__awaiter(this, void 0, void 0, function* () {
                let payload = {
                    state: 'Completed',
                    endtime: moment_1.default().valueOf(),
                    assetid: config_json_1.default.assetid,
                };
                const elementName = config_json_1.default.elements.mesoffline || 'mesoffline';
                try {
                    let query = `query=state=="${querystring_1.default.escape('In Progress')}"`;
                    const response = yield element_service_1.default.updateElementRecordsByQuery(elementName, payload, query);
                    if (response.status === UTILITY.HTTP_STATUS_CODE.SUCCESS) {
                        log.error(`Record update successfully in ShopWorx ${JSON.stringify(response.data)}`);
                    }
                    else {
                        log.error(`Error in update cyclerunning in ShopWorx ${JSON.stringify(response.data)}`);
                        UTILITY.checkSessionExpired(response.data);
                        yield UTILITY.setTimer(config_json_1.default.defaults.retryServerTimer);
                        yield this.updateRecordInSWX();
                    }
                }
                catch (error) {
                    log.error(`Exception in updating cyclerunning data in ShopWorx ${error}`);
                    yield UTILITY.setTimer(config_json_1.default.defaults.retryServerTimer);
                    yield this.updateRecordInSWX();
                }
            });
        },
    };
}
exports.default = { handshake };
