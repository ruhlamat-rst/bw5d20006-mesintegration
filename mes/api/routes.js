"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const express_1 = require("express");
const controller_1 = tslib_1.__importDefault(require("./controller"));
const router = express_1.Router();
router.post('/post-order', controller_1.default.commitOrder);
router.post('/post-componentlifealarm', controller_1.default.commitComponentLifeAlarm);
exports.default = router;
