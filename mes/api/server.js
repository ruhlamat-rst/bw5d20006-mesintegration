"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const cors_1 = tslib_1.__importDefault(require("cors"));
const Utility = tslib_1.__importStar(require("../utils/utility"));
const config_json_1 = tslib_1.__importDefault(require("../config/config.json"));
const bunyan_1 = tslib_1.__importDefault(require("bunyan"));
const express_1 = tslib_1.__importDefault(require("express"));
require("express-async-errors");
const routes_1 = tslib_1.__importDefault(require("./routes"));
let log = bunyan_1.default.createLogger({
    name: 'Api Server',
    level: config_json_1.default.logger.loglevel,
});
const app = express_1.default();
app.use(express_1.default.json());
app.use(express_1.default.urlencoded());
app.use(cors_1.default);
app.use('/api', routes_1.default);
app.use((err, req, res, next) => {
    log.error(err.message, err);
    return res.status(Utility.HTTP_STATUS_CODE.BAD_REQUEST).json({
        error: err.message,
    });
});
exports.default = { app, log };
