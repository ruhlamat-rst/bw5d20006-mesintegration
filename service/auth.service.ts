import axios, { AxiosStatic, AxiosInstance } from 'axios';
import config from '../config/config.json';
import ApiService from './api.service';

interface credential {
  identifier: string;
  password: string;
}

class AuthService {
  request: typeof ApiService;
  constructor() {
    this.request = ApiService;
  }
  authenticate(data: credential) {
    return this.request.post('/server/authenticate', data);
  }
}

const Auth = new AuthService();
export default Auth;
