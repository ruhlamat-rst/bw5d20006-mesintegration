import ApiService from './api.service';

class ServertimeService {
  request: typeof ApiService;
  constructor() {
    this.request = ApiService;
  }

  getServerTime(data: any) {
    return this.request.get('/server/servertime', data);
  }
}

const servertime = new ServertimeService();

export default servertime;
