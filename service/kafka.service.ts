import kafka from 'kafka-node';
import INTERFACE from './interface.service';
import * as UTILITY from '../utils/utility';
import Config from '../config/config.json';
import bunyan from 'bunyan';
const log = bunyan.createLogger({
  name: 'kafka',
  level: Config.logger.loglevel || 20,
});
const retryServerTimer = Config.defaults.retryServerTimer;
const maxServerRetryCount = Config.defaults.maxServerRetryCount;

const kafkaConfig = Config.kafkaEdge;
let fixedString = '';
for (var i = 0; i < kafkaConfig.fetchMaxBytes; i++) {
  fixedString += '-';
}
class KafkaService {
  kafkaClient?: kafka.KafkaClient;
  producer?: kafka.Producer;
  consumer?: kafka.ConsumerGroup;
  constructor() {}
  initClient(topic_name: string) {
    this.kafkaClient = new kafka.KafkaClient({
      kafkaHost: `${kafkaConfig.host}:${kafkaConfig.port}`,
    });
    this.kafkaClient.on('error', (error) => {
      log.error(`client error: ${error}`);
      this.initClient(topic_name);
    });
    this.initProducer();
    this.initConsumer(topic_name);
  }
  initProducer() {
    if (!this.kafkaClient) return;
    this.producer = new kafka.Producer(this.kafkaClient);
    this.producer.on(`ready`, () => {
      log.error(`Producer Ready`);
    });
    this.producer.on(`error`, (err) => {
      log.error(`[kafka-producer]: connection errored: ${err}`);
    });
  }
  initConsumer(topic_name: string) {
    if (!this.kafkaClient) return;
    let offset = new kafka.Offset(this.kafkaClient);
    let latest = 1;
    offset.fetchLatestOffsets([topic_name], function (err, offsets) {
      if (err) {
        log.error(`error fetching latest offsets from kafka topic`);
        log.error(`${err}`);
        return;
      }
      Object.keys(offsets[topic_name]).forEach((o) => {
        latest =
          offsets[topic_name][o] > latest ? offsets[topic_name][o] : latest;
      });
    });
    this.consumer = new kafka.ConsumerGroup(
      {
        kafkaHost: `${kafkaConfig.host}:${kafkaConfig.port}`,
        groupId: 'mes-' + topic_name,
        sessionTimeout: 15000,
        protocol: ['roundrobin'],
        encoding: 'utf8',
        fromOffset: kafkaConfig.fromOffset as 'earliest' | 'latest' | 'none',
        outOfRangeOffset: kafkaConfig.outOfRangeOffset as
          | 'earliest'
          | 'latest'
          | 'none',
        autoCommit: false,
        // autoCommit: kafkaConfig.autoCommit,
        // autoCommitIntervalMs: kafkaConfig.autoCommitIntervalMs,
        heartbeatInterval: 100,
        maxTickMessages: 1,
      },
      topic_name
    );
    this.consumer.on('connect', () => {
      // log.info('kafka consumerGroup connect');
    });
    this.consumer.on('offsetOutOfRange',  (error) => {
      log.error(`offsetOutOfRange ${error}`);
      this.consumer?.close(true, (err, res = {}) => {
        if (!err) {
          log.error(
            `kafka plan update event consumer connection closed successfully ${res}`
          );
        } else {
          log.error(`Error in closing kafka plan update event consumer ${err}`);
        }
        this.initConsumer(topic_name);
      });
    });
    this.consumer.on('error', (error) => {
      log.error(`Error in kafka consumer: ${error}`);
      this.consumer?.close(true, (err, res = {}) => {
        if (!err) {
          log.error(
            `kafka plan update event consumer connection closed successfully ${res}`
          );
        } else {
          log.error(`Error in closing kafka plan update event consumer ${err}`);
        }
        this.initConsumer(topic_name);
      });
    });
    this.consumer.on(`message`, async (message) => {
      const payload = JSON.parse(message.value as string);
      delete payload.ignore;
      // log.info(message.offset + " = " + latest);
      // wait for consuming messages till latestone
      if (Config.defaults.isPreviousDataIgnored) {
        if (message && message.offset && message.offset >= latest - 1) {
          this.repostCommit(payload);
        }
      } else {
        this.repostCommit(payload);
      }
    });
  }
  /**
   * This method send the payload to kafka topic
   * @param {String} topic_name
   * @param {Object} data
   */
  sendMessage(topic_name: string, data: any) {
    // check kafka producer is ready or not and then send message to kafka topic
    try {
      if (this.producer && this.producer.ready) {
        // add substring in data for fix of byte in kafka
        data.ignore = this.appendChar(data);
        const payload = [
          {
            topic: topic_name,
            messages: JSON.stringify(data),
          },
        ];
        this.producer.send(payload, (err, data) => {
          if (err) {
            log.error(`Producer Error sending data for ${topic_name} ${err}`);
          } else {
            log.info(
              `[kafka-producer -> ${topic_name}] size : ${
                JSON.stringify(payload).length
              }, payload : ${JSON.stringify(payload)} broker update success`
            );
          }
        });
      } else {
        log.error(
          `Kafka Producer is not ready. Please check kafka is configuration and kafka is running or not`
        );
      }
    } catch (ex) {
      log.error(`exeption in writing data to kafka topic ${ex}`);
    }
  }
  /**
   * This method append the empty characters to kafka payload
   * @param {Object} data
   */
  appendChar(data: any) {
    try {
      const lengthOfStr = JSON.stringify(data).length;
      // 12 length for tagname "ignore":
      const subString = fixedString.substring(
        lengthOfStr + 12,
        fixedString.length
      );
      return subString;
    } catch (ex) {
      log.error(`Exception in checking length of object ${ex}`);
    }
  }
  async repostCommit(data: any) {
    log.error(`repostCommit Payload  ${JSON.stringify(data)}`);
    try {
      const response = await (INTERFACE as any)[data.method](data.payload);
      if (response.status === UTILITY.HTTP_STATUS_CODE.SUCCESS) {
        this.consumer?.commit(() => {
          log.error(`next commit`);
        });
        log.error(`repostCommit successfully`);
      } else {
        await UTILITY.setTimer(retryServerTimer);
        await this.repostCommit(data);
      }
    } catch (error) {
      await UTILITY.setTimer(retryServerTimer);
      await this.repostCommit(data);
    }
  }
}

export default new KafkaService();
