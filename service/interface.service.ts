import axios, { AxiosResponse } from 'axios';
import Config from '../config/config.json';

interface Station {
  ProductionLineNo?: string;
  WorkCenter?: string;
  Equipment?: string;
  WipOrderNo?: string;
  SystemTime?: string;
}
interface PersonnelQualification extends Station {
  EmployeeNo: string;
}
interface EquipmentStatus extends Station {
  ReasonCode: string;
}
interface ComponentLift extends Station {
  LifeType: string;
  CountValue: number;
}
interface OrderInProduction extends Station {
  TempParentSerialNo: string;
}
interface MaterialConsumption extends OrderInProduction {
  ComponentCode: string[];
  LotNo: string[];
}
interface OrderCompleted extends OrderInProduction {
  FinalStep: string;
  Result: any;
}
interface OrderInformation extends OrderInProduction {
  ParentSerialNo: string;
}
interface ProductionInspectionData extends OrderInProduction {
  CharacteristicCode: any[];
  CharacteristicVarValue: any[];
  CharacteristicAttValue: any[];
  DefectCode: any[];
  CharacteristicResult: any[];
  WipOperStep: any[];
}
interface QualityIssue extends OrderInProduction {
  QualityIssueEList: any[];
}
interface EquipmentAlarm extends Station {
  ReasonCode: string;
  AlarmType: string;
  AlarmLevel: string;
}
interface OrderIssued extends Station {
  ProductNo: string;
  Quantity: number;
  RecipeName: string;
}
interface ComponentLifeFeedback extends Station {
  Resoncode: string;
}
interface Param<T> {
  Inputs: T;
}
const URL = `${Config.messerver.protocol}://${Config.messerver.host}/Apriso/httpServices/operations`;

class InterfaceService {
  constructor() {}
  /**
   * handshake
   */
  handshake() {
    return axios.get(`${URL}/Handshack`);
  }
  /**
   * 人员资质校验
   * @param { string } payload.EmployeeNo 操作员ID
   * @param { string } payload.WipOrderNo 生产订单号
   * @param { string } payload.WorkCenter 工作站ID
   * @param { timestamp } payload.SystemTime 时间戳
   */
  checkPersonnelQualification(
    payload: PersonnelQualification
  ): Promise<AxiosResponse<any>> {
    return axios.post(`${URL}/PersonnelQualificationVerify`, payload);
  }
  /**
   * 设备状态变更
   * @param { string } payload.ProductionLineNo 产线ID
   * @param { string } payload.State 设备状态
   * @param { string } payload.WorkCenter 工作站ID
   * @param { timestamp } payload.SystemTime 时间戳
   */
  changeEquipmentStatus(payload: EquipmentStatus): Promise<AxiosResponse<any>> {
    const param: Param<EquipmentStatus> = {
      Inputs: payload,
    };
    return axios.post(`${URL}/EquipmentStatusChange`, param);
  }

  /**
   * Order In Production  生产订单开工信息 checkin
   * @param { string } payload.ProductionLineNo 产线ID
   * @param { string } payload.WipOrderNo 生产订单号
   * @param { string } payload.WorkCenter 工作站ID
   * @param { number } payload.TempParentSerialNo 临时母件号 mainid
   * @param { timestamp } payload.SystemTime 时间戳
   */
  commitOrderInProduction(
    payload: OrderInProduction
  ): Promise<AxiosResponse<any>> {
    const param: Param<OrderInProduction> = {
      Inputs: payload,
    };
    return axios.post(`${URL}/WorkstationStartInformation`, param);
  }
  /**
   * Material Consumption 物料消耗统计 checkout
   * @param { string } payload.ProductionLineNo 产线ID
   * @param { string } payload.WipOrderNo 生产订单号
   * @param { string } payload.WorkCenter 工作站ID
   * @param { number } payload.TempParentSerialNo 临时母件号 mainid
   * @param { Array } payload.MaterialList 物料批次号合集 Batchid
   * @param { timestamp } payload.SystemTime 时间戳
   */
  commitMaterialConsumption(
    payload: MaterialConsumption
  ): Promise<AxiosResponse<any>> {
    const param: Param<MaterialConsumption> = {
      Inputs: payload,
    };
    return axios.post(`${URL}/MaterialConsumption`, param);
  }
  /**
   * The Order was Completed  生产订单过站完工信息 checkout
   * @param { string } payload.ProductionLineNo 产线ID
   * @param { string } payload.WipOrderNo 生产订单号
   * @param { string } payload.WorkCenter 工作站ID
   * @param { number } payload.TempParentSerialNo 临时母件号 mainid
   * @param { timestamp } payload.SystemTime 时间戳
   */
  commitOrderCompleted(payload: OrderCompleted): Promise<AxiosResponse<any>> {
    const param: Param<OrderCompleted> = {
      Inputs: payload,
    };
    return axios.post(`${URL}/WorkstationCompleteInformation`, param);
  }
  /**
   * Spurt Code Information Upload  生产订单合格信息 P140 checkout
   * @param { string } payload.ProductionLineNo 产线ID
   * @param { string } payload.WipOrderNo 生产订单号
   * @param { string } payload.WorkCenter 工作站ID
   * @param { number } payload.TempParentSerialNo 临时母件号 mainid
   * @param { number } payload.ParentSerialNo 合格本体号 completedid
   * @param { timestamp } payload.SystemTime 时间戳
   */
  commitOrderInformation(
    payload: OrderInformation
  ): Promise<AxiosResponse<any>> {
    const param: Param<OrderInProduction> = {
      Inputs: payload,
    };
    return axios.post(`${URL}/ProductionCompleteReport`, param);
  }
  /**
   * Production Inspection Data Upload 生产质检信息 checkout
   * @param { string } payload.ProductionLineNo 产线ID
   * @param { string } payload.WipOrderNo 生产订单号
   * @param { string } payload.WorkCenter 工作站ID
   * @param { string } payload.TempParentSerialNo 临时母件号
   * @param { Array } payload.PPList 规格参数 Params
   * @param { timestamp } payload.SystemTime 时间戳
   */
  commitProductionInspectionData(
    payload: ProductionInspectionData
  ): Promise<AxiosResponse<any>> {
    const param: Param<ProductionInspectionData> = {
      Inputs: payload,
    };
    return axios.post(`${URL}/ProductionInspectionDataUpload`, param);
  }
  /**
   * Equipment Alarm 设备报警
   * @param { string } payload.ProductionLineNo 产线ID
   * @param { string } payload.ReasonCode 故障代码
   * @param { string } payload.WorkCenter 工作站ID
   * @param { string } payload.AlarmType 报警类型
   * @param { string } payload.AlarmLevel 报警级别
   * @param { timestamp } payload.SystemTime 时间戳
   */
  commitEquipmentAlarm(payload: EquipmentAlarm): Promise<AxiosResponse<any>> {
    const param: Param<EquipmentAlarm> = {
      Inputs: payload,
    };
    return axios.post(`${URL}/EquipmentAlarm`, param);
  }
}

export default new InterfaceService();
