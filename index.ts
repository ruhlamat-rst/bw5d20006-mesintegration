'use strict';
import TAGS from './tags/tags.json';
import * as UTILITY from './utils/utility';
import { AuthJS } from './utils/auth';
import { ServerTimeJS } from './utils/servertime';
import { MasterDataJS } from './utils/masterdata';
import { SocketListenerJS } from './utils/socketlistener';
import KafkaService from './service/kafka.service';
import STATIONJS from './src/station';
import HANDSHAKEJS from './src/handshake';
import bunyan from 'bunyan';
import INTERFACE from './service/interface.service';

let CONFIG: any,
  configfilename = process.argv[2];
try {
  if (!configfilename) {
    console.log(`Error please pass correct argument of config file name`);
    process.exit(1);
  }
  let configfilepath = `./config/${configfilename}`;
  CONFIG = require(configfilepath);
} catch (ex) {
  console.log(`Exception in reading config file ${ex}`);
  process.exit(1);
}

UTILITY.validateConfigfileParameters(CONFIG);
const auth = AuthJS.auth(CONFIG, UTILITY);
const servertime = ServerTimeJS.servertime(CONFIG, UTILITY);
const MASTERDATA = MasterDataJS.masterdata(CONFIG, UTILITY, TAGS);
SocketListenerJS.socketlistener(CONFIG).init();
const emitter = SocketListenerJS.emitter;
const log = bunyan.createLogger({
  name: `index`,
  level: CONFIG.logger.loglevel,
});

const stationMap: any[] = [];
const substationTags = TAGS.substationtags;
const handshake = HANDSHAKEJS.handshake();
/**
 * Listen sessionExpired Event and authenticate again
 */
UTILITY.emitter.on('sessionExpired', () => {
  log.info(`Session Expired trying to reAuthenticate`);
  auth.getAuthentication();
});

emitter.on('substation', async (data) => {
  for (var i = 0; i < stationMap.length; i++) {
    if (data[substationTags.NAME_TAG] == stationMap[i].substationname) {
      log.error(`Substation update event triggered`);
      stationMap[i].updateStation(data);
    }
  }
});

function initStation() {
  for (var i = 0; i < MASTERDATA.stationInfo.length; i++) {
    const obj = STATIONJS.stations(
      CONFIG,
      MASTERDATA.stationInfo[i],
      UTILITY,
      TAGS,
      emitter,
      KafkaService
    );
    // wait for 5 second to get required information like element schema
    setTimeout(() => {
      // TODO initialize kafka
      obj.initEmitter();
      // start handshake
      handshake.inithandshake();
    }, 5 * 1000);
    stationMap.push(obj);
  }
}

async function getMasterData() {
  await MASTERDATA.getStationInformation();
  initStation();
}
/**
 * Authenticate E.A. before initialization
 */
AuthJS.emitter.on('init', () => {
  log.error(`Authentication done successfully`);
  servertime.getServerTime();
  getMasterData();

  
});

auth.getAuthentication();
